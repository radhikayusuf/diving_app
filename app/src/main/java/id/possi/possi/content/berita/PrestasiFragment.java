package id.possi.possi.content.berita;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import id.possi.possi.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class PrestasiFragment extends Fragment {


    public PrestasiFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_prestasi, container, false);
    }

}
