package id.possi.possi.content.location_event;


import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import id.possi.possi.R;
import id.possi.possi.dao.EventDao;

/**
 * A simple {@link Fragment} subclass.
 */
public class LocationEventFragment extends Fragment implements OnMapReadyCallback {

    private MapView mMapView;
    private Context mContext;
    private GoogleMap googleMap;
    private EventDao mData;

    private TextView textTitle, textLocation, textDesc;

    public LocationEventFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_location_event, container, false);
        initComponent(v, savedInstanceState);
        return v;
    }

    private void initComponent(View v, Bundle savedInstanceState) {
        textTitle = (TextView)v.findViewById(R.id.location_title);
        textDesc = (TextView)v.findViewById(R.id.location_desc);
        textLocation = (TextView)v.findViewById(R.id.location_alamat);


        textTitle.setText(mData.getLocation_title());
        textDesc.setText(mData.getLocation_desc());
        textLocation.setText(mData.getLocation_place());

        mMapView = (MapView) v.findViewById(R.id.myMapView);
        mContext = getActivity();
        try {
            MapsInitializer.initialize(getContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.onCreate(savedInstanceState);
        mMapView.getMapAsync(this);
        mMapView.onResume();
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext.getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        this.googleMap = googleMap;
        googleMap.setMyLocationEnabled(true);
        googleMap.setBuildingsEnabled(true);


//        if(dataNama != null){
//            for (UserModel d : dataNama) {
//                googleMap.addMarker(new MarkerOptions().position(new LatLng(d.getLat(), d.getLang())).title(d.getName()));
//            }
//        }

        LatLng home = new LatLng(mData.getLat(), mData.getLang());
        googleMap.addMarker(new MarkerOptions().position(home));
        CameraPosition cameraPosition = new CameraPosition.Builder().target(home).zoom(15).build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    public void setData(EventDao data) {
        this.mData = data;
    }
}
