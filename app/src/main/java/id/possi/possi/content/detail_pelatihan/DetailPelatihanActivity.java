package id.possi.possi.content.detail_pelatihan;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import id.possi.possi.R;
import id.possi.possi.content.detail_dive_center.DetailDiveCenterActivity;
import id.possi.possi.dao.DiveCenterDao;
import id.possi.possi.dao.PelatihanDao;

public class DetailPelatihanActivity extends AppCompatActivity {

    private PelatihanDao mData;
    private ImageView imageView;
    private TextView tvTitle, tvDesc, tvPj, tvEmail, tvTelp, tvAlamat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_pelatihan);
        initComponent();
    }

    private void initComponent() {
        imageView = (ImageView) findViewById(R.id.pelatihan_image);
        tvTitle = (TextView) findViewById(R.id.pelatihan_title);
        tvDesc = (TextView) findViewById(R.id.pelatihan_desc);
        tvEmail = (TextView) findViewById(R.id.pelatihan_email);
        tvTelp = (TextView) findViewById(R.id.pelatihan_telp);
        tvPj = (TextView) findViewById(R.id.pelatihan_pj);
        tvAlamat = (TextView) findViewById(R.id.pelatihan_alamt);


        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Pelatihan");

        if(null != getIntent()){
            Intent i = getIntent();
            mData = new PelatihanDao(
                    i.getStringExtra("id"),
                    i.getStringExtra("title"),
                    i.getStringExtra("kota"),
                    i.getStringExtra("desc"),
                    i.getStringExtra("img_url"),
                    i.getStringExtra("no_telp"),
                    i.getStringExtra("alamat"),
                    i.getStringExtra("email"),
                    i.getStringExtra("penanggung_jawab"),
                    i.getIntExtra("distance", 0),
                    i.getDoubleExtra("lat", 0),
                    i.getDoubleExtra("lang", 0)
            );
        }

        tvTitle.setText(mData.getTitle());
//        tvDesc.setText(mData.getDesc());
        tvDesc.setText(getString(R.string.long_text));
        tvEmail.setText(mData.getEmail());
        tvTelp.setText(mData.getNo_telp());
        tvPj.setText(mData.getPenanggung_jawab());
        tvAlamat.setText(mData.getAlamat());


        Picasso.with(this)
                .load(mData.getImg_url())
                .into(imageView);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public static void startThisActivity(Context context, PelatihanDao data){
        /**
         * var
         * private String id, title, kota, desc, img_url, no_telp, alamat, email, penanggung_jawab;
         * private int distance;
         * private Double lat, lang;
         */

        Intent i = new Intent(context, DetailPelatihanActivity.class);
        i.putExtra("id", data.getId());
        i.putExtra("title", data.getTitle());
        i.putExtra("kota", data.getKota());
        i.putExtra("desc", data.getDesc());
        i.putExtra("img_url", data.getImg_url());
        i.putExtra("no_telp", data.getNo_telp());
        i.putExtra("alamat", data.getAlamat());
        i.putExtra("email", data.getEmail());
        i.putExtra("penanggung_jawab", data.getPenanggung_jawab());
        i.putExtra("distance", data.getDistance());
        i.putExtra("lat", data.getLat());
        i.putExtra("lang", data.getLang());
        context.startActivity(i);
    }
}
