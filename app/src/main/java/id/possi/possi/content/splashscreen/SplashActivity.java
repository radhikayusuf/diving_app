package id.possi.possi.content.splashscreen;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import id.possi.possi.R;
import id.possi.possi.content.login.LoginActivity;

public class SplashActivity extends AppCompatActivity {

    private static int DELAY = 3000; // -> 3s
    public static String EXIT_KEY = "EXIT";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spalsh);

        if(checkIntent(getIntent())){
        new Handler().postDelayed(new Runnable() {
            /**
             * setelah 3s
             */
            @Override
            public void run() {
                startActivity(new Intent(SplashActivity.this, LoginActivity.class));
            }
        }, DELAY);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        checkIntent(getIntent());
    }

    /**
     * untuk check apakah apps akan exit
     * jadi ketika back apps tidak menampilkan kembali
     * splash screen
     * @param intent
     */
    private boolean checkIntent(Intent intent) {
        if(intent != null){
            if(intent.getStringExtra(EXIT_KEY) != null){
                finish();
                return false;
            }else{
                return true;
            }
        }else{
            return true;
        }
    }
}
