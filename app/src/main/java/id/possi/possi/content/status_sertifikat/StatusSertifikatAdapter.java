package id.possi.possi.content.status_sertifikat;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.sectionedrecyclerview.ItemCoord;
import com.afollestad.sectionedrecyclerview.SectionedRecyclerViewAdapter;
import com.afollestad.sectionedrecyclerview.SectionedViewHolder;

import java.util.ArrayList;
import java.util.List;

import id.possi.possi.R;
import id.possi.possi.content.register_pelatihan.PelatihanAdapter;
import id.possi.possi.dao.StatusSertifikatDao;
import id.possi.possi.utils.LoadMoreCallback;

/**
 * Created by root on 13/05/17.
 */

public class StatusSertifikatAdapter extends RecyclerView.Adapter<StatusSertifikatAdapter.ViewHolder> {

    public static final int TYPE_SERTIFIKAT = 0;
    public static final int TYPE_LOAD_MORE = 1;

    private List<StatusSertifikatDao> mData = new ArrayList<>();
    private LoadMoreCallback callback;
    private Context mContext;

    private Dialog mDialog;

    public StatusSertifikatAdapter(List<StatusSertifikatDao> mData, LoadMoreCallback callback) {
        this.mData = mData;
        this.callback = callback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View v = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        return new ViewHolder(v, R.layout.row_sertifikat == viewType ? TYPE_SERTIFIKAT : TYPE_LOAD_MORE);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        if(holder.type == TYPE_SERTIFIKAT){
            holder.title.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    builder.setView(R.layout.dialog_sertifikat);
                    builder.setCancelable(true);
                    mDialog = builder.create();
                    mDialog.show();

                    Button b = (Button) mDialog.findViewById(R.id.btn_close_dialog);
                    b.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mDialog.dismiss();
                        }
                    });


                    return false;
                }
            });
        }else{
            holder.v.getRootView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holder.loadmore.setText("Muat Lainnya");
                    holder.loadmore.setVisibility(View.GONE);
                    holder.progressBar.setVisibility(View.VISIBLE);
                    callback.onClickLoadmore(holder.progressBar, holder.loadmore);
                }
            });
        }

    }

    @Override
    public int getItemViewType(int position) {
        if(position != mData.size()){
            return R.layout.row_sertifikat;
        }else{
            return R.layout.loadmore_nocard_row;
        }

//        return super.getItemViewType(position);

    }

    @Override
    public int getItemCount() {
        return mData != null ? mData.size() + 1 : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView title, loadmore;
        ProgressBar progressBar;
        public View v;
        public int type = 0;

        public ViewHolder(View itemView, int type) {
            super(itemView);
            v = itemView;
            this.type = type;

            if(type == TYPE_SERTIFIKAT){
                title = (TextView)itemView.findViewById(R.id.tv_title_sertifikat);
            }else{
                loadmore = (TextView) v.findViewById(R.id.load_more);
                progressBar = (ProgressBar) v.findViewById(R.id.progress_bar);
            }

        }
    }
}
