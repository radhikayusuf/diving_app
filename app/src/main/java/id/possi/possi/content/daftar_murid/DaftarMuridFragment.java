package id.possi.possi.content.daftar_murid;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import id.possi.possi.R;
import id.possi.possi.dao.DaftarSiswaDao;
import id.possi.possi.utils.LoadMoreCallback;
import id.possi.possi.utils.MyDialog;


public class DaftarMuridFragment extends Fragment implements LoadMoreCallback, MyDialog.CallbackDialogStatus {

//    private OnDaftarMUridFragmentInteractionListener mListener;
    private RecyclerView rc_daftar_siswa;
    private List<DaftarSiswaDao> mData = new ArrayList<>();
    private LinearLayoutManager layoutManager;
    private DaftarMuridAdapter adapter;

    public DaftarMuridFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_daftar_murid, container, false);
        initComponent(v);
        return v;
    }

    private void initComponent(View v) {
        rc_daftar_siswa = (RecyclerView) v.findViewById(R.id.rc_daftar_siswa);
        layoutManager = new LinearLayoutManager(getContext());
        adapter = new DaftarMuridAdapter(mData, this, this);

        rc_daftar_siswa.setAdapter(adapter);
        rc_daftar_siswa.setLayoutManager(layoutManager);
        //Data Dummy
        mData.add(new DaftarSiswaDao(1,"Edward","edward.nana@gmail.com","L","30-05-1999","Bandung", "Margahayu", "Jawa Barat", "http://bootstrap.gallery/everest-v3/img/user4.jpg", false));
        mData.add(new DaftarSiswaDao(2,"Cyntia","cyntia.nana@gmail.com","L","30-05-1999","Bandung", "Margahayu", "Jawa Barat", "http://bootstrap.gallery/everest-v3/img/user1.jpg", true));
        mData.add(new DaftarSiswaDao(3,"Lala","lala.nana@gmail.com","L","30-05-1999","Bandung", "Margahayu", "Jawa Barat", "http://bootstrap.gallery/everest-v3/img/user4.jpg",true));
        mData.add(new DaftarSiswaDao(4,"Rayhan","rayhan@gmail.com","L","30-05-1999","Bandung", "Margahayu", "Jawa Barat", "http://bootstrap.gallery/everest-v3/img/user2.jpg",false));
        mData.add(new DaftarSiswaDao(4,"Rayhan","rayhan@gmail.com","L","30-05-1999","Bandung", "Margahayu", "Jawa Barat", "http://bootstrap.gallery/everest-v3/img/user2.jpg",false));
        mData.add(new DaftarSiswaDao(4,"Rayhan","rayhan@gmail.com","L","30-05-1999","Bandung", "Margahayu", "Jawa Barat", "http://bootstrap.gallery/everest-v3/img/user2.jpg",false));
        mData.add(new DaftarSiswaDao(4,"Rayhan","rayhan@gmail.com","L","30-05-1999","Bandung", "Margahayu", "Jawa Barat", "http://bootstrap.gallery/everest-v3/img/user2.jpg",false));
        mData.add(new DaftarSiswaDao(4,"Rayhan","rayhan@gmail.com","L","30-05-1999","Bandung", "Margahayu", "Jawa Barat", "http://bootstrap.gallery/everest-v3/img/user2.jpg",false));
        mData.add(new DaftarSiswaDao(5,"Ridwan","ridwan@gmail.com","L","30-05-1999","Bandung", "Margahayu", "Jawa Barat", "http://bootstrap.gallery/everest-v3/img/user3.jpg",true));
        mData.add(new DaftarSiswaDao(6,"Udin","udin@gmail.com","L","30-05-1999","Bandung", "Margahayu", "Jawa Barat", "http://bootstrap.gallery/everest-v3/img/user5.jpg",false));

        adapter.notifyDataSetChanged();
    }


    //@Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
////        if (context instanceof OnFragmentInteractionListener) {
////            mListener = (OnFragmentInteractionListener) context;
////        } else {
////            throw new RuntimeException(context.toString()
////                    + " must implement OnFragmentInteractionListener");
////        }
//    }

    @Override
    public void onDetach() {
        super.onDetach();
//        mListener = null;
    }

    @Override
    public void onClickLoadmore(ProgressBar progressBar, TextView loadmore) {
        mData.add(new DaftarSiswaDao(7,"Gaga","rayhan@gmail.com","L","30-05-1999","Bandung", "Margahayu", "Jawa Barat", "http://bootstrap.gallery/everest-v3/img/user2.jpg",false));
        mData.add(new DaftarSiswaDao(8,"Budi","ridwan@gmail.com","L","30-05-1999","Bandung", "Margahayu", "Jawa Barat", "http://bootstrap.gallery/everest-v3/img/user3.jpg",true));
        mData.add(new DaftarSiswaDao(9,"Henhen","udin@gmail.com","L","30-05-1999","Bandung", "Margahayu", "Jawa Barat", "http://bootstrap.gallery/everest-v3/img/user5.jpg",false));

        adapter.notifyDataSetChanged();
    }

    @Override
    public void onChangeStatus(int pos, boolean status) {
        mData.get(pos).setStatus_lunas(status ? true : false);
        adapter.notifyDataSetChanged();
    }

//    public interface OnDaftarMUridFragmentInteractionListener {
//        void onFragmentInteraction(Uri uri);
//    }
}
