package id.possi.possi.content.berita;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;

import java.util.HashMap;

import id.possi.possi.R;
import id.possi.possi.content.event.EventFragment;
import id.possi.possi.content.reward.RewardFragment;


public class BeritaFragment extends Fragment implements AppBarLayout.OnOffsetChangedListener, BeritaInterface, SwipeRefreshLayout.OnRefreshListener {


    private SliderLayout sliderLayout;

    private NestedScrollView scrollView;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private AppBarLayout appBarLayout;
    private SwipeRefreshLayout swipeRefreshLayout;
    private BeritaPagerAdapter pagerAdapter;
    private HashMap<String, String> slider_data = new HashMap<>();

    private ContentBeritaFragment contentBeritaFragment;
    private RewardFragment rewardFragment;
    private EventFragment eventFragment;

    private boolean isTop = false;

    public BeritaFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_berita, container, false);
        initComponent(v);
        return v;
    }
//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }


    private void initComponent(View v) {
        sliderLayout = (SliderLayout) v.findViewById(R.id.slider_berita);
        tabLayout = (TabLayout) v.findViewById(R.id.tab_berita);
        viewPager = (ViewPager) v.findViewById(R.id.pager_berita);
        appBarLayout = (AppBarLayout) v.findViewById(R.id.appbar1);
        swipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.myswipe);

        contentBeritaFragment = new ContentBeritaFragment();
        contentBeritaFragment.setBeritaInterface(this);
        rewardFragment = new RewardFragment();
        rewardFragment.setBeritaInterface(this);
        eventFragment = new EventFragment();
        eventFragment.setBeritaInterface(this);

        pagerAdapter = new BeritaPagerAdapter(getFragmentManager());
        pagerAdapter.addFragment(contentBeritaFragment, "");
        pagerAdapter.addFragment(rewardFragment, "");
        pagerAdapter.addFragment(eventFragment, "");

        viewPager.setOffscreenPageLimit(3);

        viewPager.setAdapter(pagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

        int tabTitles[] = {R.string.news_tab, R.string.reward_tab, R.string.event_tab};
        int tabIcons[] = {R.drawable.ic_library_books_white_24dp, R.drawable.ic_star_white_24dp, R.drawable.ic_date_range_black_18dp};

        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            LinearLayout tabLinearLayout = (LinearLayout) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
            tabLinearLayout.setGravity(Gravity.CENTER_HORIZONTAL);
            tabLinearLayout.setBackgroundColor(Color.TRANSPARENT);
            TextView tabContent = (TextView) tabLinearLayout.findViewById(R.id.tabContent);
            tabContent.setText("  "+getActivity().getResources().getString(tabTitles[i]));
            tabContent.setCompoundDrawablesWithIntrinsicBounds(tabIcons[i], 0, 0, 0);
            tabLayout.getTabAt(i).setCustomView(tabContent);
        }

        slider_data.put("Pemenang PON selam","https://cdns.klimg.com/merdeka.com/i/w/news/2016/09/16/754975/540x270/selam-jatim-panen-empat-emas-di-pon-2016-20160916213510.jpg");
        slider_data.put("Latihan selam serentak","http://3.bp.blogspot.com/-ryCoP2T9bLY/VjlrogVSg4I/AAAAAAAABwo/dGUKB2--2Q8/s1600/1446601729435.jpg");

        for (String s : slider_data.keySet()) {
            TextSliderView sliderView = new TextSliderView(getContext());

            if(slider_data.get(s) != null){
                sliderView
                    .description(s)
                    .image(slider_data.get(s))
                    .setScaleType(BaseSliderView.ScaleType.CenterCrop);
            }else{
                sliderView
                    .description(s)
                    .image(R.drawable.img_placeholder)
                    .setScaleType(BaseSliderView.ScaleType.CenterCrop);
            }


            sliderLayout.addSlider(sliderView);
        }

        swipeRefreshLayout.setOnRefreshListener(this);


        //scrollView.scrollTo(0, scrollView.getTop());
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
        if(!swipeRefreshLayout.isRefreshing()){
            if(isTop && i ==0){
                swipeRefreshLayout.setEnabled(true);
            }else{
                swipeRefreshLayout.setEnabled(false);
            }
        }


    }

    @Override
    public void onResume() {
        super.onResume();
        appBarLayout.addOnOffsetChangedListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        appBarLayout.removeOnOffsetChangedListener(this);
    }


    @Override
    public void isOnTop(boolean isTop) {
        this.isTop = isTop;
    }

    @Override
    public void onRefresh() {
        switch (tabLayout.getSelectedTabPosition()) {
            case 0:
                contentBeritaFragment.refreshBerita(swipeRefreshLayout);
                break;
            case 1:
                rewardFragment.refreshBerita(swipeRefreshLayout);
                break;
            default:
                eventFragment.refreshBerita(swipeRefreshLayout);
                break;
        }

    }


    public int getSelectedTab(){
        return tabLayout.getSelectedTabPosition();
    }

    public void updateFilter(String provinsi, int dialog_type) {
        switch (dialog_type) {
            case 0:
                contentBeritaFragment.updateRecycler(provinsi);
                break;
            case 1:
                rewardFragment.updateRecycler(provinsi);
                break;
            case 2:
                eventFragment.updateRecycler(provinsi);
                break;
        }
    }
}
