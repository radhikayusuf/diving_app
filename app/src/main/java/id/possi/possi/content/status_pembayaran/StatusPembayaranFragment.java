package id.possi.possi.content.status_pembayaran;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import id.possi.possi.R;
import id.possi.possi.dao.StatusPembayaranDao;
import id.possi.possi.utils.LoadMoreCallback;

public class StatusPembayaranFragment extends Fragment implements LoadMoreCallback {

    private RecyclerView rc_pembayaran;
    private LinearLayoutManager layoutManager;
    private StatusPembayaranAdapter adapter;
    private List<StatusPembayaranDao> mData = new ArrayList<>();


    public StatusPembayaranFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_status_pembayaran, container, false);
        initComponent(v);
        return v;
    }

    private void initComponent(View v) {
        rc_pembayaran = (RecyclerView) v.findViewById(R.id.rc_pembayaran);
        layoutManager = new LinearLayoutManager(getContext());
        adapter = new StatusPembayaranAdapter(mData, this);

        rc_pembayaran.setLayoutManager(layoutManager);
        rc_pembayaran.setAdapter(adapter);

        mData.add(new StatusPembayaranDao(1, 100000, "Latihan Tahunan", "lorem ipsum", true));
        mData.add(new StatusPembayaranDao(1, 502000, "Latihan Tahunan", "lorem ipsum", true));
        mData.add(new StatusPembayaranDao(1, 30000, "Latihan Tahunan", "lorem ipsum", true));
        mData.add(new StatusPembayaranDao(1, 600000, "Latihan Tahunan", "lorem ipsum", true));
        mData.add(new StatusPembayaranDao(1, 700000, "Latihan Tahunan", "lorem ipsum", true));
        mData.add(new StatusPembayaranDao(1, 200000, "Latihan Tahunan", "lorem ipsum", true));
        mData.add(new StatusPembayaranDao(1, 400000, "Latihan Tahunan", "lorem ipsum", true));
        mData.add(new StatusPembayaranDao(1, 8700000, "Latihan Tahunan", "lorem ipsum", true));

        adapter.notifyDataSetChanged();

    }

    @Override
    public void onClickLoadmore(final ProgressBar progressBar, final TextView loadmore) {
        new Handler().postDelayed(new Runnable() {
            /**
             * setelah 3s
             */
            @Override
            public void run() {
                progressBar.setVisibility(View.GONE);
                loadmore.setVisibility(View.VISIBLE);
                loadMore();
            }
        }, 3000);
    }

    private void loadMore() {
        mData.add(new StatusPembayaranDao(1, 100000, "Latihan Tahunan", "lorem ipsum", true));
        mData.add(new StatusPembayaranDao(1, 502000, "Latihan Tahunan", "lorem ipsum", false));
        mData.add(new StatusPembayaranDao(1, 30000, "Latihan Tahunan", "lorem ipsum", false));
        adapter.notifyDataSetChanged();
    }


//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
////        if (context instanceof OnFragmentInteractionListener) {
////            mListener = (OnFragmentInteractionListener) context;
////        } else {
////            throw new RuntimeException(context.toString()
////                    + " must implement OnFragmentInteractionListener");
////        }
//    }

}
