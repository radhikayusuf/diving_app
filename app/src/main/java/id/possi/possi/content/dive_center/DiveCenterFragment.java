package id.possi.possi.content.dive_center;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import id.possi.possi.R;
import id.possi.possi.dao.DiveCenterDao;
import id.possi.possi.utils.LoadMoreCallback;

/**
 * A simple {@link Fragment} subclass.
 */
public class DiveCenterFragment extends Fragment implements LoadMoreCallback {

    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private DiveCenterAdapter adapter;
    private List<DiveCenterDao> mData = new ArrayList<>();

    public DiveCenterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_dive_center, container, false);
        initComponent(v);

        return v;
    }

    private void initComponent(View v) {
        recyclerView = (RecyclerView) v.findViewById(R.id.rc_dive_center);
        layoutManager = new LinearLayoutManager(getContext());
        adapter = new DiveCenterAdapter(mData, this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        
        mData.add(new DiveCenterDao("1","Kolam renang Bikasoga", "Bandung", "Kolam renang yang di fasilitasi dengan baik", "http://bandung.panduanwisata.id/files/2013/03/kolam-renang-bandung2.jpg","0831873262","Buah Batu", "bikasoga@mail.com","Bapak Sam", 30, -6.3212, 107.3213));
        mData.add(new DiveCenterDao("1","Kolam renang Bikasoga", "Bandung", "Kolam renang yang di fasilitasi dengan baik", "http://bandung.panduanwisata.id/files/2013/03/kolam-renang-bandung2.jpg","0831873262","Buah Batu", "bikasoga@mail.com","Bapak Sam", 30, -6.3212, 107.3213));
        mData.add(new DiveCenterDao("1","Kolam renang Bikasoga", "Bandung", "Kolam renang yang di fasilitasi dengan baik", "http://bandung.panduanwisata.id/files/2013/03/kolam-renang-bandung2.jpg","0831873262","Buah Batu", "bikasoga@mail.com","Bapak Sam", 30, -6.3212, 107.3213));
        mData.add(new DiveCenterDao("1","Kolam renang Bikasoga", "Bandung", "Kolam renang yang di fasilitasi dengan baik", "http://bandung.panduanwisata.id/files/2013/03/kolam-renang-bandung2.jpg","0831873262","Buah Batu", "bikasoga@mail.com","Bapak Sam", 30, -6.3212, 107.3213));
        mData.add(new DiveCenterDao("1","Kolam renang Bikasoga", "Bandung", "Kolam renang yang di fasilitasi dengan baik", "http://bandung.panduanwisata.id/files/2013/03/kolam-renang-bandung2.jpg","0831873262","Buah Batu", "bikasoga@mail.com","Bapak Sam", 30, -6.3212, 107.3213));

        adapter.notifyDataSetChanged();
    }

    @Override
    public void onClickLoadmore(ProgressBar progressBar, TextView loadmore) {
        mData.add(new DiveCenterDao("1","New Kolam renang Bikasoga", "Bandung", "Kolam renang yang di fasilitasi dengan baik", "http://bandung.panduanwisata.id/files/2013/03/kolam-renang-bandung2.jpg","0831873262","Buah Batu", "bikasoga@mail.com","Bapak Sam", 30, -6.3212, 107.3213));
        mData.add(new DiveCenterDao("1","New Kolam renang Bikasoga", "Bandung", "Kolam renang yang di fasilitasi dengan baik", "http://bandung.panduanwisata.id/files/2013/03/kolam-renang-bandung2.jpg","0831873262","Buah Batu", "bikasoga@mail.com","Bapak Sam", 30, -6.3212, 107.3213));
        adapter.notifyDataSetChanged();
    }
}
