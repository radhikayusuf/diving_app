package id.possi.possi.content.detail_event;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import id.possi.possi.R;
import id.possi.possi.content.detail_berita.DetailBeritaActivity;
import id.possi.possi.content.dive_center.DiveCenterFragment;
import id.possi.possi.content.information_event.InformationEventFragment;
import id.possi.possi.content.location_event.LocationEventFragment;
import id.possi.possi.dao.BeritaDao;
import id.possi.possi.dao.EventDao;
import id.possi.possi.utils.Helper;

public class DetailEventActivity extends AppCompatActivity implements OnMapReadyCallback {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private EventDao mData;
    private DetailEventPagerAdapter pagerAdapter;
    private InformationEventFragment informationEventFragment;
    private LocationEventFragment locationEventFragment;
    private FragmentTransaction fragmentTransaction;
    private TextView textEventTitle, textEventDate, textEventDesc, textLocationTitle, textLocationPlace, textLocationDesc;;
    private ImageView imageView;
    private MapView mMapView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_event);
        initComponent();

        initComponentFragment(savedInstanceState);
    }

    private void initComponent() {
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        informationEventFragment = (InformationEventFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_information);

        if(null != getIntent()){
            Intent i = getIntent();
            mData = new EventDao(i.getStringExtra("id"),
                    i.getStringExtra("title"),
                    i.getStringExtra("location_desc"),
                    i.getStringExtra("desc"),
                    i.getStringExtra("img"),
                    i.getStringExtra("location_title"),
                    i.getStringExtra("location_place"),
                    i.getStringExtra("provinsi"),
                    i.getLongExtra("datetime", 0),
                    i.getDoubleExtra("lat", 0),
                    i.getDoubleExtra("lang",0));
            informationEventFragment.setData(mData);
        }

        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.show(informationEventFragment);
        fragmentTransaction.commit();

//        tabLayout = (TabLayout) findViewById(R.id.tab_event);
//        viewPager = (ViewPager) findViewById(R.id.pager_event);
//        pagerAdapter = new DetailEventPagerAdapter(getSupportFragmentManager());
//
//        locationEventFragment = new LocationEventFragment();
//
//        informationEventFragment.setData(mData);
//        locationEventFragment.setData(mData);
//
//        pagerAdapter.addFragment(informationEventFragment, "INFO");
//        pagerAdapter.addFragment(locationEventFragment, "LOKASI");
//
//        viewPager.setAdapter(pagerAdapter);
//        tabLayout.setupWithViewPager(viewPager);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Event");



    }

    public static void startThisActivity(Context context, EventDao data){
        /**
         * var EventDao
         * private String id, title, location_desc, desc, img, location_title, location_place;
         * private long datetime;
         * private double lat, lang;
         */

        Intent i = new Intent(context, DetailEventActivity.class);
        i.putExtra("id", data.getId());
        i.putExtra("title", data.getTitle());
        i.putExtra("location_desc", data.getLocation_desc());
        i.putExtra("desc", data.getDesc());
        i.putExtra("img", data.getImg());
        i.putExtra("location_title", data.getLocation_title());
        i.putExtra("location_place", data.getLocation_place());
        i.putExtra("provinsi", data.getLokasi_provinsi());
        i.putExtra("datetime", data.getDatetime());
        i.putExtra("lat", data.getLat());
        i.putExtra("lang", data.getLang());
        context.startActivity(i);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initComponentFragment(Bundle savedInstanceState) {
        imageView = (ImageView)findViewById(R.id.img_event);
        textEventTitle = (TextView)findViewById(R.id.title_event);
        textEventDate = (TextView)findViewById(R.id.datetime_event);
        textEventDesc = (TextView)findViewById(R.id.desc_event);

        textLocationTitle = (TextView)findViewById(R.id.location_title);
        textLocationDesc = (TextView)findViewById(R.id.location_desc);
        textLocationPlace = (TextView)findViewById(R.id.location_alamat);


        Picasso.with(this)
                .load(mData.getImg())
                .into(imageView);

        textEventTitle.setText(mData.getTitle());
        textEventDesc.setText(mData.getDesc());

        textLocationTitle.setText(mData.getLocation_title());
        textLocationDesc.setText(mData.getLocation_desc());
        textLocationPlace.setText(mData.getLocation_place());

        textEventDate.setText(Helper.longToStringDateFormat(mData.getDatetime()));

        mMapView = (MapView) findViewById(R.id.myMapView);
        try {
            MapsInitializer.initialize(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.onCreate(savedInstanceState);
        mMapView.getMapAsync(this);
        mMapView.onResume();

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (ActivityCompat.checkSelfPermission(DetailEventActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(DetailEventActivity.this.getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

//        this.googleMap = googleMap;
        googleMap.setMyLocationEnabled(true);
        googleMap.setBuildingsEnabled(true);


//        if(dataNama != null){
//            for (UserModel d : dataNama) {
//                googleMap.addMarker(new MarkerOptions().position(new LatLng(d.getLat(), d.getLang())).title(d.getName()));
//            }
//        }

        LatLng home = new LatLng(mData.getLat(), mData.getLang());
        googleMap.addMarker(new MarkerOptions().position(home));
        CameraPosition cameraPosition = new CameraPosition.Builder().target(home).zoom(15).build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }
}
