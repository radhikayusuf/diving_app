package id.possi.possi.content.detail_berita;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import id.possi.possi.R;
import id.possi.possi.dao.BeritaDao;

public class DetailBeritaActivity extends AppCompatActivity {

    private ImageView image_news;
    private TextView title, creator, date, content;
    private BeritaDao mData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_berita);
        initComponent();
        if(getIntent() != null){
            loadDataToView();
        }
    }

    private void loadDataToView() {
        Intent i = getIntent();
        mData = new BeritaDao(i.getIntExtra("id",0), i.getLongExtra("date", 0),
                i.getStringExtra("creator"), i.getStringExtra("title"),
                i.getStringExtra("img"), i.getStringExtra("content"), i.getStringExtra("provinsi"));

        Picasso.with(this)
                .load(mData.getUrl_image())
                .into(image_news);

        SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM dd yyyy", Locale.getDefault());
        
        Date buff_date = new Date();
        buff_date.setTime(mData.getDate());

        date.setText("Di terbitkan : "+String.valueOf(dateFormat.format(buff_date)));
        title.setText(mData.getTitle());
        creator.setText("Ditulis Oleh : "+mData.getCreator());
        content.setText(mData.getContent());
    }

    private void initComponent() {
        image_news = (ImageView) findViewById(R.id.image_news_detail);
        title = (TextView) findViewById(R.id.title_news);
        creator = (TextView) findViewById(R.id.creator);
        date = (TextView) findViewById(R.id.date_news);
        content = (TextView) findViewById(R.id.text_content);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Berita");

    }

    public static void startThisActivity(Context context, BeritaDao data){
        Intent i = new Intent(context, DetailBeritaActivity.class);
        i.putExtra("id", data.getId());
        i.putExtra("date", data.getDate());
        i.putExtra("title", data.getTitle());
        i.putExtra("creator", data.getCreator());
        i.putExtra("img", data.getUrl_image());
        i.putExtra("content", data.getContent());
        i.putExtra("provinsi", data.getLokasi_provinsi());
        context.startActivity(i);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
