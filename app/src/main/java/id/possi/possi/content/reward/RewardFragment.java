package id.possi.possi.content.reward;


import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import id.possi.possi.R;
import id.possi.possi.content.berita.BeritaFragment;
import id.possi.possi.content.berita.BeritaInterface;
import id.possi.possi.dao.RewardDao;
import id.possi.possi.utils.LoadMoreCallback;

/**
 * A simple {@link Fragment} subclass.
 */
public class RewardFragment extends Fragment implements LoadMoreCallback, NestedScrollView.OnScrollChangeListener {

    private RecyclerView recyclerView;
    private NestedScrollView nestedScrollView;
    private List<RewardDao> mData = new ArrayList<>();
    private List<RewardDao> mDataOriginal = new ArrayList<>();
    private LinearLayoutManager layoutManager;
    private RewardAdapter adapter;
    private BeritaInterface evt;


    public RewardFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_reward, container, false);
        initComponent(v);
        return v;
    }

    private void initComponent(View v) {
        recyclerView = (RecyclerView)v.findViewById(R.id.rc_reward);
        nestedScrollView = (NestedScrollView) v.findViewById(R.id.nested_reward);

        nestedScrollView.setOnScrollChangeListener(this);
        layoutManager = new LinearLayoutManager(getContext());
        adapter = new RewardAdapter(mData, this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setAdapter(adapter);

        mData.add(new RewardDao(1, 1493101710000L, "Admin", "Jabar - 285 Atlet Berprestasi Dapat Reward dari Pemerintah", "http://banyuwangikab.go.id/media/berita/images/._.JPG","BANYUWANGI –Sebagai bentuk perhatian kepada atlet dan pelatih yang telah berhasil meengukir prestasi baik ditingkat internasional, nasional maupun lokal, Pemkab Banyuwangi memberikan reward kepada 285 atlet dan 54 pelatih. Reward yang dirupakan dalam bentuk rupiah diserahkan Bupati Banyuwangi, Abdullah Azwar Anas di Pendopo Banyuwangi, Senin (16/2) sebelum peluncuran maskot porprov 2015.\n" +
                "\n" +
                "Dari 285 atlet yang mendapatkan reward, tiga atlet secara simbolis menerima reward dari Bupati Anas. Mereka adalah Rendiyana Putra, peraih medali emas International Asian Youth Games, dari cabang lari estafet 4 kali 100 meter, di Vietnam. Syafira Nur Islami, peraih medali emas, Kejurnas Selam 2014 dengan jarak 1500 meter. Tasya Nabila, Juara I Kerjuda Taekwondo. Sementara untuk pelatih ada Suyatmi (25), pelatih senam yang berhasil melahirkan banyak atlet senam tingkat nasional.  ", "Jawa Barat"));
        mData.add(new RewardDao(1, 1493101710000L, "Admin", "Yogya -285 Atlet Berprestasi Dapat Reward dari Pemerintah", "http://banyuwangikab.go.id/media/berita/images/._.JPG","BANYUWANGI –Sebagai bentuk perhatian kepada atlet dan pelatih yang telah berhasil meengukir prestasi baik ditingkat internasional, nasional maupun lokal, Pemkab Banyuwangi memberikan reward kepada 285 atlet dan 54 pelatih. Reward yang dirupakan dalam bentuk rupiah diserahkan Bupati Banyuwangi, Abdullah Azwar Anas di Pendopo Banyuwangi, Senin (16/2) sebelum peluncuran maskot porprov 2015.\n" +
                "\n" +
                "Dari 285 atlet yang mendapatkan reward, tiga atlet secara simbolis menerima reward dari Bupati Anas. Mereka adalah Rendiyana Putra, peraih medali emas International Asian Youth Games, dari cabang lari estafet 4 kali 100 meter, di Vietnam. Syafira Nur Islami, peraih medali emas, Kejurnas Selam 2014 dengan jarak 1500 meter. Tasya Nabila, Juara I Kerjuda Taekwondo. Sementara untuk pelatih ada Suyatmi (25), pelatih senam yang berhasil melahirkan banyak atlet senam tingkat nasional.  ", "Daerah Istimewa Yogyakarta"));
        mData.add(new RewardDao(1, 1493101710000L, "Admin", "Bali - 285 Atlet Berprestasi Dapat Reward dari Pemerintah", "http://banyuwangikab.go.id/media/berita/images/._.JPG","BANYUWANGI –Sebagai bentuk perhatian kepada atlet dan pelatih yang telah berhasil meengukir prestasi baik ditingkat internasional, nasional maupun lokal, Pemkab Banyuwangi memberikan reward kepada 285 atlet dan 54 pelatih. Reward yang dirupakan dalam bentuk rupiah diserahkan Bupati Banyuwangi, Abdullah Azwar Anas di Pendopo Banyuwangi, Senin (16/2) sebelum peluncuran maskot porprov 2015.\n" +
                "\n" +
                "Dari 285 atlet yang mendapatkan reward, tiga atlet secara simbolis menerima reward dari Bupati Anas. Mereka adalah Rendiyana Putra, peraih medali emas International Asian Youth Games, dari cabang lari estafet 4 kali 100 meter, di Vietnam. Syafira Nur Islami, peraih medali emas, Kejurnas Selam 2014 dengan jarak 1500 meter. Tasya Nabila, Juara I Kerjuda Taekwondo. Sementara untuk pelatih ada Suyatmi (25), pelatih senam yang berhasil melahirkan banyak atlet senam tingkat nasional.  ", "Bali"));
        mData.add(new RewardDao(1, 1493101710000L, "Admin", "Yogya - 285 Atlet Berprestasi Dapat Reward dari Pemerintah", "http://banyuwangikab.go.id/media/berita/images/._.JPG","BANYUWANGI –Sebagai bentuk perhatian kepada atlet dan pelatih yang telah berhasil meengukir prestasi baik ditingkat internasional, nasional maupun lokal, Pemkab Banyuwangi memberikan reward kepada 285 atlet dan 54 pelatih. Reward yang dirupakan dalam bentuk rupiah diserahkan Bupati Banyuwangi, Abdullah Azwar Anas di Pendopo Banyuwangi, Senin (16/2) sebelum peluncuran maskot porprov 2015.\n" +
                "\n" +
                "Dari 285 atlet yang mendapatkan reward, tiga atlet secara simbolis menerima reward dari Bupati Anas. Mereka adalah Rendiyana Putra, peraih medali emas International Asian Youth Games, dari cabang lari estafet 4 kali 100 meter, di Vietnam. Syafira Nur Islami, peraih medali emas, Kejurnas Selam 2014 dengan jarak 1500 meter. Tasya Nabila, Juara I Kerjuda Taekwondo. Sementara untuk pelatih ada Suyatmi (25), pelatih senam yang berhasil melahirkan banyak atlet senam tingkat nasional.  ", "Daerah Istimewa Yogyakarta"));
        mData.add(new RewardDao(1, 1493101710000L, "Admin", "Jabar - 285 Atlet Berprestasi Dapat Reward dari Pemerintah", "http://banyuwangikab.go.id/media/berita/images/._.JPG","BANYUWANGI –Sebagai bentuk perhatian kepada atlet dan pelatih yang telah berhasil meengukir prestasi baik ditingkat internasional, nasional maupun lokal, Pemkab Banyuwangi memberikan reward kepada 285 atlet dan 54 pelatih. Reward yang dirupakan dalam bentuk rupiah diserahkan Bupati Banyuwangi, Abdullah Azwar Anas di Pendopo Banyuwangi, Senin (16/2) sebelum peluncuran maskot porprov 2015.\n" +
                "\n" +
                "Dari 285 atlet yang mendapatkan reward, tiga atlet secara simbolis menerima reward dari Bupati Anas. Mereka adalah Rendiyana Putra, peraih medali emas International Asian Youth Games, dari cabang lari estafet 4 kali 100 meter, di Vietnam. Syafira Nur Islami, peraih medali emas, Kejurnas Selam 2014 dengan jarak 1500 meter. Tasya Nabila, Juara I Kerjuda Taekwondo. Sementara untuk pelatih ada Suyatmi (25), pelatih senam yang berhasil melahirkan banyak atlet senam tingkat nasional.  ", "Jawa Barat"));
        mData.add(new RewardDao(1, 1493101710000L, "Admin", "Yogya - 285 Atlet Berprestasi Dapat Reward dari Pemerintah", "http://banyuwangikab.go.id/media/berita/images/._.JPG","BANYUWANGI –Sebagai bentuk perhatian kepada atlet dan pelatih yang telah berhasil meengukir prestasi baik ditingkat internasional, nasional maupun lokal, Pemkab Banyuwangi memberikan reward kepada 285 atlet dan 54 pelatih. Reward yang dirupakan dalam bentuk rupiah diserahkan Bupati Banyuwangi, Abdullah Azwar Anas di Pendopo Banyuwangi, Senin (16/2) sebelum peluncuran maskot porprov 2015.\n" +
                "\n" +
                "Dari 285 atlet yang mendapatkan reward, tiga atlet secara simbolis menerima reward dari Bupati Anas. Mereka adalah Rendiyana Putra, peraih medali emas International Asian Youth Games, dari cabang lari estafet 4 kali 100 meter, di Vietnam. Syafira Nur Islami, peraih medali emas, Kejurnas Selam 2014 dengan jarak 1500 meter. Tasya Nabila, Juara I Kerjuda Taekwondo. Sementara untuk pelatih ada Suyatmi (25), pelatih senam yang berhasil melahirkan banyak atlet senam tingkat nasional.  ", "Daerah Istimewa Yogyakarta"));
        adapter.notifyDataSetChanged();

        mDataOriginal.addAll(mData);
    }

    @Override
    public void onClickLoadmore(final ProgressBar progressBar, final TextView loadmore) {
        new Handler().postDelayed(new Runnable() {
            /**
             * setelah 3s
             */
            @Override
            public void run() {
                progressBar.setVisibility(View.GONE);
                loadmore.setVisibility(View.VISIBLE);
                loadMore();
            }
        }, 3000);

    }

    private void loadMore() {
        mData.add(new RewardDao(1, 1493101710000L, "Admin", "Bali - 285 Atlet Berprestasi Dapat Reward dari Pemerintah", "http://banyuwangikab.go.id/media/berita/images/._.JPG","BANYUWANGI –Sebagai bentuk perhatian kepada atlet dan pelatih yang telah berhasil meengukir prestasi baik ditingkat internasional, nasional maupun lokal, Pemkab Banyuwangi memberikan reward kepada 285 atlet dan 54 pelatih. Reward yang dirupakan dalam bentuk rupiah diserahkan Bupati Banyuwangi, Abdullah Azwar Anas di Pendopo Banyuwangi, Senin (16/2) sebelum peluncuran maskot porprov 2015.\n" +
                "\n" +
                "Dari 285 atlet yang mendapatkan reward, tiga atlet secara simbolis menerima reward dari Bupati Anas. Mereka adalah Rendiyana Putra, peraih medali emas International Asian Youth Games, dari cabang lari estafet 4 kali 100 meter, di Vietnam. Syafira Nur Islami, peraih medali emas, Kejurnas Selam 2014 dengan jarak 1500 meter. Tasya Nabila, Juara I Kerjuda Taekwondo. Sementara untuk pelatih ada Suyatmi (25), pelatih senam yang berhasil melahirkan banyak atlet senam tingkat nasional.  ", "Bali"));
        mData.add(new RewardDao(1, 1493101710000L, "Admin", "Jabar - 285 Atlet Berprestasi Dapat Reward dari Pemerintah", "http://banyuwangikab.go.id/media/berita/images/._.JPG","BANYUWANGI –Sebagai bentuk perhatian kepada atlet dan pelatih yang telah berhasil meengukir prestasi baik ditingkat internasional, nasional maupun lokal, Pemkab Banyuwangi memberikan reward kepada 285 atlet dan 54 pelatih. Reward yang dirupakan dalam bentuk rupiah diserahkan Bupati Banyuwangi, Abdullah Azwar Anas di Pendopo Banyuwangi, Senin (16/2) sebelum peluncuran maskot porprov 2015.\n" +
                "\n" +
                "Dari 285 atlet yang mendapatkan reward, tiga atlet secara simbolis menerima reward dari Bupati Anas. Mereka adalah Rendiyana Putra, peraih medali emas International Asian Youth Games, dari cabang lari estafet 4 kali 100 meter, di Vietnam. Syafira Nur Islami, peraih medali emas, Kejurnas Selam 2014 dengan jarak 1500 meter. Tasya Nabila, Juara I Kerjuda Taekwondo. Sementara untuk pelatih ada Suyatmi (25), pelatih senam yang berhasil melahirkan banyak atlet senam tingkat nasional.  ", "Jawa Barat"));
        mData.add(new RewardDao(1, 1493101710000L, "Admin", "Bali - 285 Atlet Berprestasi Dapat Reward dari Pemerintah", "http://banyuwangikab.go.id/media/berita/images/._.JPG","BANYUWANGI –Sebagai bentuk perhatian kepada atlet dan pelatih yang telah berhasil meengukir prestasi baik ditingkat internasional, nasional maupun lokal, Pemkab Banyuwangi memberikan reward kepada 285 atlet dan 54 pelatih. Reward yang dirupakan dalam bentuk rupiah diserahkan Bupati Banyuwangi, Abdullah Azwar Anas di Pendopo Banyuwangi, Senin (16/2) sebelum peluncuran maskot porprov 2015.\n" +
                "\n" +
                "Dari 285 atlet yang mendapatkan reward, tiga atlet secara simbolis menerima reward dari Bupati Anas. Mereka adalah Rendiyana Putra, peraih medali emas International Asian Youth Games, dari cabang lari estafet 4 kali 100 meter, di Vietnam. Syafira Nur Islami, peraih medali emas, Kejurnas Selam 2014 dengan jarak 1500 meter. Tasya Nabila, Juara I Kerjuda Taekwondo. Sementara untuk pelatih ada Suyatmi (25), pelatih senam yang berhasil melahirkan banyak atlet senam tingkat nasional.  ", "Bali"));

        adapter.notifyDataSetChanged();
    }

    public void setBeritaInterface(BeritaFragment beritaInterface) {
        this.evt = beritaInterface;
    }

    public void refreshBerita(final SwipeRefreshLayout swipeRefreshLayout) {
        mData.clear();

        mData.add(new RewardDao(1, 1493101710000L, "Admin", "Bali - 285 Atlet Berprestasi Dapat Reward dari Pemerintah", "http://banyuwangikab.go.id/media/berita/images/._.JPG","BANYUWANGI –Sebagai bentuk perhatian kepada atlet dan pelatih yang telah berhasil meengukir prestasi baik ditingkat internasional, nasional maupun lokal, Pemkab Banyuwangi memberikan reward kepada 285 atlet dan 54 pelatih. Reward yang dirupakan dalam bentuk rupiah diserahkan Bupati Banyuwangi, Abdullah Azwar Anas di Pendopo Banyuwangi, Senin (16/2) sebelum peluncuran maskot porprov 2015.\n" +
                "\n" +
                "Dari 285 atlet yang mendapatkan reward, tiga atlet secara simbolis menerima reward dari Bupati Anas. Mereka adalah Rendiyana Putra, peraih medali emas International Asian Youth Games, dari cabang lari estafet 4 kali 100 meter, di Vietnam. Syafira Nur Islami, peraih medali emas, Kejurnas Selam 2014 dengan jarak 1500 meter. Tasya Nabila, Juara I Kerjuda Taekwondo. Sementara untuk pelatih ada Suyatmi (25), pelatih senam yang berhasil melahirkan banyak atlet senam tingkat nasional.  ", "Bali"));
        mData.add(new RewardDao(1, 1493101710000L, "Admin", "Jabar - 285 Atlet Berprestasi Dapat Reward dari Pemerintah", "http://banyuwangikab.go.id/media/berita/images/._.JPG","BANYUWANGI –Sebagai bentuk perhatian kepada atlet dan pelatih yang telah berhasil meengukir prestasi baik ditingkat internasional, nasional maupun lokal, Pemkab Banyuwangi memberikan reward kepada 285 atlet dan 54 pelatih. Reward yang dirupakan dalam bentuk rupiah diserahkan Bupati Banyuwangi, Abdullah Azwar Anas di Pendopo Banyuwangi, Senin (16/2) sebelum peluncuran maskot porprov 2015.\n" +
                "\n" +
                "Dari 285 atlet yang mendapatkan reward, tiga atlet secara simbolis menerima reward dari Bupati Anas. Mereka adalah Rendiyana Putra, peraih medali emas International Asian Youth Games, dari cabang lari estafet 4 kali 100 meter, di Vietnam. Syafira Nur Islami, peraih medali emas, Kejurnas Selam 2014 dengan jarak 1500 meter. Tasya Nabila, Juara I Kerjuda Taekwondo. Sementara untuk pelatih ada Suyatmi (25), pelatih senam yang berhasil melahirkan banyak atlet senam tingkat nasional.  ", "Jawa Barat"));
        mData.add(new RewardDao(1, 1493101710000L, "Admin", "Jabar -285 Atlet Berprestasi Dapat Reward dari Pemerintah", "http://banyuwangikab.go.id/media/berita/images/._.JPG","BANYUWANGI –Sebagai bentuk perhatian kepada atlet dan pelatih yang telah berhasil meengukir prestasi baik ditingkat internasional, nasional maupun lokal, Pemkab Banyuwangi memberikan reward kepada 285 atlet dan 54 pelatih. Reward yang dirupakan dalam bentuk rupiah diserahkan Bupati Banyuwangi, Abdullah Azwar Anas di Pendopo Banyuwangi, Senin (16/2) sebelum peluncuran maskot porprov 2015.\n" +
                "\n" +
                "Dari 285 atlet yang mendapatkan reward, tiga atlet secara simbolis menerima reward dari Bupati Anas. Mereka adalah Rendiyana Putra, peraih medali emas International Asian Youth Games, dari cabang lari estafet 4 kali 100 meter, di Vietnam. Syafira Nur Islami, peraih medali emas, Kejurnas Selam 2014 dengan jarak 1500 meter. Tasya Nabila, Juara I Kerjuda Taekwondo. Sementara untuk pelatih ada Suyatmi (25), pelatih senam yang berhasil melahirkan banyak atlet senam tingkat nasional.  ", "Jawa Barat"));
        adapter.notifyDataSetChanged();

        new Handler().postDelayed(new Runnable() {
            /**
             * setelah 3s
             */
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
            }
        }, 3000);
    }

    @Override
    public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
        if(null != v && evt != null){
            if (scrollY < 20) {
                Log.wtf("onScrollChangeEvent: ", "true");
                evt.isOnTop(true);
            }else{
                evt.isOnTop(false);
                Log.wtf("onScrollChangeEvent: ", "false");
            }
        }
    }

    public void updateRecycler(String provinsi) {
        List<RewardDao> buffData = new ArrayList<>();

        if(!provinsi.equalsIgnoreCase("--")){
            for (RewardDao rewardDao : mData) {
                if(rewardDao.getLokasi_provinsi().equalsIgnoreCase(provinsi))buffData.add(rewardDao);
            }
            mData.clear();
            mData.addAll(buffData);
        }else{
            mData.clear();
            mData.addAll(mDataOriginal);
        }

        adapter.notifyDataSetChanged();
    }
}
