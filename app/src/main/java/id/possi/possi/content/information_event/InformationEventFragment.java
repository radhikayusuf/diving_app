package id.possi.possi.content.information_event;


import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import id.possi.possi.R;
import id.possi.possi.dao.EventDao;

/**
 * A simple {@link Fragment} subclass.
 */
public class InformationEventFragment extends Fragment implements OnMapReadyCallback {

    private EventDao mData;
    private MapView mMapView;
    private Context mContext;
    private GoogleMap googleMap;
    private TextView textEventTitle, textEventDate, textEventDesc, textLocationTitle, textLocationPlace, textLocationDesc;;
    private ImageView imageView;

    public InformationEventFragment() {
        // Required empty public constructor
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_information_event, container, false);;
        //initComponent(v, savedInstanceState);
        return v;
    }

    private void initComponent(View v, Bundle savedInstanceState) {
        imageView = (ImageView)v.findViewById(R.id.img_event);
        textEventTitle = (TextView)v.findViewById(R.id.title_event);
        textEventDate = (TextView)v.findViewById(R.id.datetime_event);
        textEventDesc = (TextView)v.findViewById(R.id.desc_event);
        textLocationTitle = (TextView)v.findViewById(R.id.location_title);
        textLocationDesc = (TextView)v.findViewById(R.id.location_desc);
        textLocationPlace = (TextView)v.findViewById(R.id.location_alamat);


        Picasso.with(getActivity())
                .load(mData.getImg())
                .into(imageView);

        textEventTitle.setText(mData.getTitle());
        textEventDesc.setText(mData.getDesc());

        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, MMM dd yy, HH:mm", Locale.getDefault());
        Date buff_date = new Date();
        buff_date.setTime(mData.getDatetime());

        textEventDate.setText(String.valueOf(dateFormat.format(buff_date)));

        mMapView = (MapView) v.findViewById(R.id.myMapView);
        mContext = getActivity();
        try {
            MapsInitializer.initialize(getContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.onCreate(savedInstanceState);
        mMapView.getMapAsync(this);
        mMapView.onResume();

    }

    public void setData(EventDao data) {
        this.mData = data;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext.getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        this.googleMap = googleMap;
        googleMap.setMyLocationEnabled(true);
        googleMap.setBuildingsEnabled(true);


//        if(dataNama != null){
//            for (UserModel d : dataNama) {
//                googleMap.addMarker(new MarkerOptions().position(new LatLng(d.getLat(), d.getLang())).title(d.getName()));
//            }
//        }

        LatLng home = new LatLng(mData.getLat(), mData.getLang());
        googleMap.addMarker(new MarkerOptions().position(home));
        CameraPosition cameraPosition = new CameraPosition.Builder().target(home).zoom(15).build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }
}
