package id.possi.possi.content.detail_reward;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import id.possi.possi.R;
import id.possi.possi.content.detail_berita.DetailBeritaActivity;
import id.possi.possi.dao.BeritaDao;
import id.possi.possi.dao.RewardDao;
import id.possi.possi.utils.Helper;

public class DetailRewardActivity extends AppCompatActivity {

    private ImageView image_news;
    private TextView title, creator, date, content;
    private RewardDao mData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_reward);
        initComponent();
        loadDataToView();
    }

    private void loadDataToView() {
        Intent i = getIntent();
        mData = new RewardDao(i.getIntExtra("id",0), i.getLongExtra("date", 0),
                i.getStringExtra("creator"), i.getStringExtra("title"),
                i.getStringExtra("img"), i.getStringExtra("content"), i.getStringExtra("provinsi"));

        Picasso.with(this)
                .load(mData.getUrl_image())
                .into(image_news);
        date.setText("Di terbitkan : "+ Helper.longToStringCustomDateFormat(mData.getDate(), "MMMM dd yyyy"));
        title.setText(mData.getTitle());
        creator.setText("Ditulis Oleh : "+mData.getCreator());
        content.setText(mData.getContent());
    }


    private void initComponent() {
        image_news = (ImageView) findViewById(R.id.image_reward_detail);
        title = (TextView) findViewById(R.id.title_reward);
        creator = (TextView) findViewById(R.id.creator_reward);
        date = (TextView) findViewById(R.id.date_reward);
        content = (TextView) findViewById(R.id.text_content_reward);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Reward");

    }

    public static void startThisActivity(Context context, RewardDao data){
        Intent i = new Intent(context, DetailRewardActivity.class);
        i.putExtra("id", data.getId());
        i.putExtra("date", data.getDate());
        i.putExtra("title", data.getTitle());
        i.putExtra("creator", data.getCreator());
        i.putExtra("img", data.getUrl_image());
        i.putExtra("content", data.getContent());
        i.putExtra("provinsi", data.getLokasi_provinsi());
        context.startActivity(i);
    }
}
