package id.possi.possi.content.dive_center;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import id.possi.possi.R;
import id.possi.possi.content.detail_dive_center.DetailDiveCenterActivity;
import id.possi.possi.dao.DiveCenterDao;
import id.possi.possi.utils.LoadMoreCallback;

/**
 * Created by root on 04/05/17.
 */

public class DiveCenterAdapter extends RecyclerView.Adapter<DiveCenterAdapter.ViewHolder> {

    public static final int TYPE_DIVE = 0;
    public static final int TYPE_LOAD_MORE = 1;

    private List<DiveCenterDao> mData = new ArrayList<>();
    private LoadMoreCallback callback;
    private Context mContext;

    public DiveCenterAdapter(List<DiveCenterDao> mData, LoadMoreCallback callback) {
        this.mData = mData;
        this.callback = callback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View v = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        return new ViewHolder(v, R.layout.row_dive_center == viewType ? TYPE_DIVE : TYPE_LOAD_MORE);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        if(holder.type == TYPE_DIVE){
            holder.title.setText(mData.get(position).getTitle());
            holder.location.setText(mData.get(position).getKota());
            holder.distance.setText("Distance "+mData.get(position).getDistance()+" KM");

            Picasso.with(mContext)
                    .load(mData.get(position).getImg_url())
                    .into(holder.imageView);

            holder.v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DetailDiveCenterActivity.startThisActivity(mContext, mData.get(position));
                }
            });
        }else{
            holder.loadmore.setText("Muat Lainnya");
            holder.v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holder.loadmore.setVisibility(View.GONE);
                    holder.progressBar.setVisibility(View.VISIBLE);
                    callback.onClickLoadmore(holder.progressBar, holder.loadmore);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mData != null ? mData.size() + 1 : 0;
    }

    @Override
    public int getItemViewType(int position) {
        if(position != mData.size()){
            return R.layout.row_dive_center;
        }else{
            return R.layout.loadmore_nocard_row;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView title, location, distance, loadmore;
        ProgressBar progressBar;
        View v;
        int type = 0;
        public ViewHolder(View itemView, int i) {
            super(itemView);
            v = itemView;
            type = i;
            if(type == TYPE_DIVE){
                imageView = (ImageView)v.findViewById(R.id.image_row_dive);
                title = (TextView) v.findViewById(R.id.title_row_dive);
                location = (TextView)v.findViewById(R.id.location_row_dive);
                distance = (TextView)v.findViewById(R.id.distance_row_dive);
            }else{
                loadmore = (TextView)v.findViewById(R.id.load_more);
                progressBar = (ProgressBar) v.findViewById(R.id.progress_bar);
            }


        }
    }
}
