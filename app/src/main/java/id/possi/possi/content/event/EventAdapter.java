package id.possi.possi.content.event;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import id.possi.possi.R;
import id.possi.possi.content.detail_event.DetailEventActivity;
import id.possi.possi.dao.EventDao;
import id.possi.possi.utils.Helper;
import id.possi.possi.utils.LoadMoreCallback;

/**
 * Created by root on 03/05/17.
 */

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.ViewHolder> {

    public static final int TYPE_EVENT = 0;
    public static final int TYPE_LOAD_MORE = 2;

    private Context context;
    private List<EventDao> mData = new ArrayList<>();
    private LoadMoreCallback callback;

    public EventAdapter(List<EventDao> mData, LoadMoreCallback callback) {
        this.mData = mData;
        this.callback = callback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        this.context = parent.getContext();
        return new ViewHolder(v, viewType == R.layout.row_event ? TYPE_EVENT: TYPE_LOAD_MORE);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        if(holder.type == TYPE_EVENT){
            holder.title.setText(mData.get(position).getTitle());
            holder.location.setText(mData.get(position).getLocation_place());
            holder.date.setText(Helper.longToStringCustomDateFormat(mData.get(position).getDatetime(), "EEE, MMM yy, HH:mm"));
            Picasso.with(context)
                    .load(mData.get(position).getImg())
                    .into(holder.imageView);

            holder.rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DetailEventActivity.startThisActivity(context, mData.get(position));
                }
            });
        }else{
            holder.loadmore.setText("Event Lainnya");
            holder.rootView.getRootView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holder.loadmore.setVisibility(View.GONE);
                    holder.progressBar.setVisibility(View.VISIBLE);
                    callback.onClickLoadmore(holder.progressBar, holder.loadmore);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mData != null ? mData.size() + 1: 0;
    }

    @Override
    public int getItemViewType(int position) {
        if(position != mData.size()){
            return R.layout.row_event;
        }else{
            return R.layout.loadmore_nocard_row;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView title, location, date, loadmore;
        ProgressBar progressBar;
        View rootView;
        int type = 0;

        public ViewHolder(View itemView, int i) {
            super(itemView);
            type = i;
            rootView = itemView;
            if(type == TYPE_EVENT){
                imageView = (ImageView) itemView.findViewById(R.id.img_event);
                title = (TextView) itemView.findViewById(R.id.event_name);
                date = (TextView) itemView.findViewById(R.id.event_date);
                location = (TextView) itemView.findViewById(R.id.event_location);
            }else{
                loadmore = (TextView) itemView.findViewById(R.id.load_more);
                progressBar = (ProgressBar) itemView.findViewById(R.id.progress_bar);
            }
        }

    }
}
