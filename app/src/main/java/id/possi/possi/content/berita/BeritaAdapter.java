package id.possi.possi.content.berita;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import id.possi.possi.R;
import id.possi.possi.content.detail_berita.DetailBeritaActivity;
import id.possi.possi.dao.BeritaDao;
import id.possi.possi.utils.Helper;
import id.possi.possi.utils.LoadMoreCallback;

/**
 * Created by root on 11/04/17.
 */

public class BeritaAdapter extends RecyclerView.Adapter<BeritaAdapter.ViewHolder> {

    private List<BeritaDao> mData = new ArrayList<>();
    private Context context;

    public static int TYPE_NEWS = 0;
    public static int TYPE_LOAD_MORE = 1;

    private LoadMoreCallback evt;

    public BeritaAdapter(List<BeritaDao> mData, LoadMoreCallback evt) {
        this.mData = mData;
        this.evt = evt;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View v =  LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        return new ViewHolder(v, viewType == R.layout.news_row ? TYPE_NEWS: TYPE_LOAD_MORE);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        if(holder.type == TYPE_NEWS){
            holder.title.setText(mData.get(position).getTitle());
            holder.date.setText(String.valueOf(Helper.longToStringDateFormat(mData.get(position).getDate())));


            Picasso.with(context)
                    .load(mData.get(position).getUrl_image())
                    .placeholder(R.drawable.img_placeholder)
                    .into(holder.img);


            holder.v.getRootView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DetailBeritaActivity.startThisActivity(context, mData.get(position));
                }
            });
        }else{
            holder.v.getRootView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holder.loadmore.setVisibility(View.GONE);
                    holder.progressBar.setVisibility(View.VISIBLE);
                    evt.onClickLoadmore(holder.progressBar, holder.loadmore);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return mData != null ? mData.size() + 1 : 0;
    }

    @Override
    public int getItemViewType(int position) {
        if(position != mData.size()){
            return R.layout.news_row;
        }else{
            return R.layout.loadmore_nocard_row;
        }

//        return super.getItemViewType(position);

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img;
        TextView date, title, loadmore;
        ProgressBar progressBar;
        public View v;
        public int type = 0;

        public ViewHolder(View v, int type) {
            super(v);
            this.v = v;
            this.type = type;

            if(type == TYPE_NEWS){
                img = (ImageView) v.findViewById(R.id.img_news_row);
                title = (TextView) v.findViewById(R.id.content_news_row);
                date = (TextView) v.findViewById(R.id.title_news_row);
            }else{
                loadmore = (TextView) v.findViewById(R.id.load_more);
                progressBar = (ProgressBar) v.findViewById(R.id.progress_bar);
            }

        }
    }

}
