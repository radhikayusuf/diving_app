package id.possi.possi.content.edit_profile;

import android.support.v4.app.ActivityCompat;

import java.lang.ref.WeakReference;

import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.PermissionUtils;

/**
 * Created by root on 12/05/17.
 */

final class EditProfileDispatcher {
    private static final int REQUEST_DOSTARTCAMERA = 4;

    private static final String[] PERMISSION_DOSTARTCAMERA = new String[] {"android.permission.CAMERA"};

    private static final int REQUEST_DOSTARTPICKIMAGE = 5;

    private static final String[] PERMISSION_DOSTARTPICKIMAGE = new String[] {"android.permission.READ_EXTERNAL_STORAGE"};

    private EditProfileDispatcher() {
    }

    static void doStartCameraWithCheck(EditProfileActivity target) {
        if (PermissionUtils.hasSelfPermissions(target, PERMISSION_DOSTARTCAMERA)) {
            target.doStartCamera();
        } else {
            if (PermissionUtils.shouldShowRequestPermissionRationale(target, PERMISSION_DOSTARTCAMERA)) {
                target.showRationaleForCamera(new DoStartCameraPermissionRequest(target).getPermissionRequest());
            } else {
                ActivityCompat.requestPermissions(target, PERMISSION_DOSTARTCAMERA, REQUEST_DOSTARTCAMERA);
            }
        }
    }

    static void doStartPickImageWithCheck(EditProfileActivity target) {
        if (PermissionUtils.hasSelfPermissions(target, PERMISSION_DOSTARTPICKIMAGE)) {
            target.doStartPickImage();
        } else {
            ActivityCompat.requestPermissions(target, PERMISSION_DOSTARTPICKIMAGE, REQUEST_DOSTARTPICKIMAGE);
        }
    }

    static void onRequestPermissionsResult(EditProfileActivity target, int requestCode, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_DOSTARTCAMERA:
                if (PermissionUtils.getTargetSdkVersion(target) < 23 && !PermissionUtils.hasSelfPermissions(target, PERMISSION_DOSTARTCAMERA)) {
                    return;
                }
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    target.doStartCamera();
                } else {
                    if (!PermissionUtils.shouldShowRequestPermissionRationale(target, PERMISSION_DOSTARTCAMERA)) {
                        target.showNeverAskForCamera();
                    }
                }
                break;
            case REQUEST_DOSTARTPICKIMAGE:
                if (PermissionUtils.getTargetSdkVersion(target) < 23 && !PermissionUtils.hasSelfPermissions(target, PERMISSION_DOSTARTPICKIMAGE)) {
                    target.showDeniedForReadExternalStorage();
                    return;
                }
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    target.doStartPickImage();
                } else {
                    target.showDeniedForReadExternalStorage();
                }
                break;
            default:
                break;
        }
    }

    private static final class DoStartCameraPermissionRequest implements PermissionRequest {
        private final WeakReference<EditProfileActivity> weakTarget;
        DoStartCameraPermissionRequest permissionRequest;

        private DoStartCameraPermissionRequest(EditProfileActivity target) {
            this.weakTarget = new WeakReference<>(target);
            permissionRequest = this;
        }

        public DoStartCameraPermissionRequest getPermissionRequest() {
            return permissionRequest;
        }

        @Override
        public void proceed() {
            EditProfileActivity target = weakTarget.get();
            if (target == null) return;
            ActivityCompat.requestPermissions(target, PERMISSION_DOSTARTCAMERA, REQUEST_DOSTARTCAMERA);
        }

        @Override
        public void cancel() {
        }
    }
}
