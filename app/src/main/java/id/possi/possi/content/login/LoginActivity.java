package id.possi.possi.content.login;

import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import id.possi.possi.R;
import id.possi.possi.content.main.MainActivity;
import id.possi.possi.content.sign_up.RegisterActivity;
import id.possi.possi.content.splashscreen.SplashActivity;

public class LoginActivity extends AppCompatActivity implements View.OnFocusChangeListener {
    private TextInputEditText mEditRUser, mEditRPassword;
    private ScrollView scrollView;
    private TextView signUp, tvlewati;
    private Button btnLogin;
    private boolean isOpened = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_v2);
        initComponent();
        focusToEditText();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(SplashActivity.EXIT_KEY, "exit");
        startActivity(intent);
    }

    @Override
    protected void onStart() {
        super.onStart();
        setListnerToRootView();
    }

    private void initComponent() {
        mEditRUser = (TextInputEditText) findViewById(R.id.edit_rUser);
        mEditRPassword = (TextInputEditText) findViewById(R.id.edit_rPassword);

        scrollView = (ScrollView) findViewById(R.id.scroll_login);
        btnLogin = (Button) findViewById(R.id.btnLogin);

        signUp = (TextView) findViewById(R.id.etSignUp);
        tvlewati = (TextView) findViewById(R.id.tv_lewati);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, MainActivity.class));
            }
        });

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
            }
        });

        tvlewati.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, MainActivity.class));
            }
        });
    }

    private void focusToEditText(){
//        scrollView.scrollTo(0, mEditRPassword.getBottom());
//        if(mEditRUser.requestFocus() || mEditRUser.requestFocus()){
//            scrollView.post(new Runnable() {
//                @Override
//                public void run() {
//                    scrollView.scrollTo(0, btnLogin.getBottom());
//                }
//            });
//        }

//        mEditRUser.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View view, boolean b) {
//                if(view.getId() == mEditRUser.getId() || view.getId() == mEditRUser.getId()){
//                    scrollView.post(new Runnable() {
//                        @Override
//                        public void run() {
//                            scrollView.scrollTo(0, scrollView.getBottom());
//                        }
//                    });
//                }
//            }
//        });
//
//        mEditRPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View view, boolean b) {
//                if(view.getId() == mEditRUser.getId() || view.getId() == mEditRUser.getId()){
//                    scrollView.post(new Runnable() {
//                        @Override
//                        public void run() {
//                            scrollView.scrollTo(0, scrollView.getBottom());
//                        }
//                    });
//                }
//            }
//        });
        mEditRUser.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b){
                    Handler handler;
                    handler = new Handler();

                    Runnable r = new Runnable() {
                        public void run() {
                            scrollView.scrollTo(0, signUp.getBottom());
                        }
                    };
                    handler.postDelayed(r, 200);
                }
            }
        });
        mEditRPassword.setOnFocusChangeListener(this);
    }

    public void setListnerToRootView() {
        final View activityRootView = getWindow().getDecorView().findViewById(android.R.id.content);
        activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int a = activityRootView.getRootView().getHeight();
                int b = activityRootView.getHeight();
                int heightDiff = a - b;
                if (heightDiff > 100) {
                    scrollView.post(new Runnable() {
                        @Override
                        public void run() {
                            scrollView.scrollTo(0, signUp.getBottom());
                        }
                    });

                    if (!isOpened) {
                    }
                    isOpened = true;
                } else if (isOpened) {
                    isOpened = false;
                }
            }
        });
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        if (!b){
            Handler handler;
            handler = new Handler();

            Runnable r = new Runnable() {
                public void run() {
                    scrollView.scrollTo(0, signUp.getBottom());
                }
            };
            handler.postDelayed(r, 200);
        }
    }
}
