package id.possi.possi.content.status_sertifikat;


import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import id.possi.possi.R;
import id.possi.possi.dao.StatusSertifikatDao;
import id.possi.possi.utils.LoadMoreCallback;

/**
 * A simple {@link Fragment} subclass.
 */
public class StatusSertifikatFragment extends Fragment implements LoadMoreCallback {

    private RecyclerView recyclerView;
    private StatusSertifikatAdapter adapter;
    private List<StatusSertifikatDao> mData = new ArrayList<>();

    public StatusSertifikatFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_status_sertifikat, container, false);
        initComponent(v);
        return v;
    }

    private void initComponent(View v) {
        recyclerView = (RecyclerView) v.findViewById(R.id.rc_sertifikat);
        adapter = new StatusSertifikatAdapter(mData, this);
        LinearLayoutManager manager =
                new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);

        mData.add(new StatusSertifikatDao());
        mData.add(new StatusSertifikatDao());
        mData.add(new StatusSertifikatDao());
        mData.add(new StatusSertifikatDao());
        mData.add(new StatusSertifikatDao());
        mData.add(new StatusSertifikatDao());
        mData.add(new StatusSertifikatDao());
        mData.add(new StatusSertifikatDao());
        mData.add(new StatusSertifikatDao());
        mData.add(new StatusSertifikatDao());

        adapter.notifyDataSetChanged();
    }

    @Override
    public void onClickLoadmore(final ProgressBar progressBar, final TextView loadmore) {
        new Handler().postDelayed(new Runnable() {
            /**
             * setelah 3s
             */
            @Override
            public void run() {
                progressBar.setVisibility(View.GONE);
                loadmore.setVisibility(View.VISIBLE);
                loadMore();
            }
        }, 3000);
    }

    private void loadMore() {
        mData.add(new StatusSertifikatDao());
        mData.add(new StatusSertifikatDao());

        adapter.notifyDataSetChanged();
    }
}
