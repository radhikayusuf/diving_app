package id.possi.possi.content.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import id.possi.possi.R;
import id.possi.possi.content.about.AboutActivity;
import id.possi.possi.content.berita.BeritaFragment;
import id.possi.possi.content.daftar_murid.DaftarMuridFragment;
import id.possi.possi.content.daftar_pelatihan.DaftarPelatihanFragment;
import id.possi.possi.content.dive_center.DiveCenterFragment;
import id.possi.possi.content.edit_profile.EditProfileActivity;
import id.possi.possi.content.pengaduan.PengaduanFragment;
import id.possi.possi.content.profile.ProfileFragment;
import id.possi.possi.content.register_pelatihan.RegisterPelatihanFragment;
import id.possi.possi.content.splashscreen.SplashActivity;
import id.possi.possi.content.status_pembayaran.StatusPembayaranFragment;
import id.possi.possi.content.status_sertifikat.StatusSertifikatFragment;
import id.possi.possi.utils.FilterDialog;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, FilterDialog.DialogCallback {

    private Fragment fragment;
    private FragmentTransaction fragmentTransaction;

    private Menu menu;
    private Toolbar toolbar;
    private NavigationView navigationView;

    private StatusPembayaranFragment statusPembayaranFragment;
    private RegisterPelatihanFragment registerPelatihanFragment;
    private DaftarPelatihanFragment daftarPelatihanFragment;
    private DaftarMuridFragment daftarMuridFragment;
    private BeritaFragment beritaFragment;
    private DiveCenterFragment diveCenterFragment;
    private ProfileFragment profileFragment;
    private StatusSertifikatFragment statusSertifikatFragment;
    private PengaduanFragment pengaduanFragment;

    private FilterDialog filterDialogBerita, filterDialogReward, filterDialogEvent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initComponent();

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
            Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra(SplashActivity.EXIT_KEY, "exit");
            startActivity(intent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.main, menu);
        getMenuInflater().inflate(R.menu.filter_menu, menu);
        this.menu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        switch (id) {
            case R.id.save_profile:
                EditProfileActivity.startActivity(this);
                break;
            case R.id.filter_main:
                if(beritaFragment.getSelectedTab() == 0){
                    filterDialogBerita.setDialogTitle("Filter Berita");
                    filterDialogBerita.setEvt(this);
                    filterDialogBerita.setType_dialog(0);
                    filterDialogBerita.show(getFragmentManager(), "");
                }else if(beritaFragment.getSelectedTab() == 1){
                    filterDialogReward.setDialogTitle("Filter Reward");
                    filterDialogReward.setEvt(this);
                    filterDialogReward.setType_dialog(1);
                    filterDialogReward.show(getFragmentManager(), "");
                }else{
                    filterDialogEvent.setDialogTitle("Filter Reward");
                    filterDialogEvent.setEvt(this);
                    filterDialogEvent.setType_dialog(2);
                    filterDialogEvent.show(getFragmentManager(), "");
                }

                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        boolean result = true;

        getMenuInflater().inflate(R.menu.save_profile, menu);

        toolbar.getMenu().clear();

        if (item.isChecked()) item.setChecked(false);
        else item.setChecked(true);

        if (id == R.id.nav_home) {
            fragment = beritaFragment;
            getSupportActionBar().setTitle(getApplicationContext().getString(R.string.app_name));
            getMenuInflater().inflate(R.menu.filter_menu, menu);
        } else if (id == R.id.nav_profile) {
            fragment = profileFragment;
            getSupportActionBar().setTitle("Profile");
            getMenuInflater().inflate(R.menu.save_profile, menu);
        } else if (id == R.id.nav_murid) {
            fragment = daftarMuridFragment;
            getSupportActionBar().setTitle("Daftar Murid");
        } else if (id == R.id.nav_pembayaran) {
            fragment = statusPembayaranFragment;
            getSupportActionBar().setTitle("Status Pembayaran");
        } else if (id == R.id.nav_daftar) {
            fragment = registerPelatihanFragment;
            getSupportActionBar().setTitle("Registrasi Pelatihan");
        } else if (id == R.id.nav_pelatihan) {
            fragment = daftarPelatihanFragment;
            getSupportActionBar().setTitle("Daftar Pelatihan");
        } else if(id == R.id.nav_status_sertifikasi){
            fragment = statusSertifikatFragment;
            getSupportActionBar().setTitle("Status Sertifikat");
        } else if(id == R.id.nav_pengaduan_kehilangan_kartu){
            fragment = pengaduanFragment;
            getSupportActionBar().setTitle("Pengaduan Kehilangan kartu");
        } else if (id == R.id.nav_dive_center) {
            fragment = diveCenterFragment;
            getSupportActionBar().setTitle("Dive Center");
        } else if (id == R.id.nav_logout) {
            if (navigationView.getMenu().getItem(9).getSubMenu().getItem(1).isChecked()) {
                navigationView.getMenu().getItem(9).getSubMenu().getItem(1).setChecked(false);
            }
            onBackPressed();
            result = false;
        } else if (id == R.id.nav_about) {
            if (navigationView.getMenu().getItem(9).getSubMenu().getItem(0).isChecked()) {
                navigationView.getMenu().getItem(9).getSubMenu().getItem(0).setChecked(false);
            }
            startActivity(new Intent(this, AboutActivity.class));
            result = false;
        }

        if (fragment != null) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.hide(statusPembayaranFragment);
            fragmentTransaction.hide(registerPelatihanFragment);
            fragmentTransaction.hide(daftarMuridFragment);
            fragmentTransaction.hide(daftarPelatihanFragment);
            fragmentTransaction.hide(beritaFragment);
            fragmentTransaction.hide(profileFragment);
            fragmentTransaction.hide(diveCenterFragment);
            fragmentTransaction.hide(statusSertifikatFragment);
            fragmentTransaction.hide(pengaduanFragment);
            fragmentTransaction.show(fragment).addToBackStack(fragment.getClass().getSimpleName());
            fragmentTransaction.commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return result;
    }

    private void initComponent() {
        statusPembayaranFragment = (StatusPembayaranFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_status);
        registerPelatihanFragment = (RegisterPelatihanFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_register_pelatihan);
        daftarPelatihanFragment = (DaftarPelatihanFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_daftar_pelatihan);
        daftarMuridFragment = (DaftarMuridFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentMurid);
        beritaFragment = (BeritaFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_berita);
        profileFragment = (ProfileFragment) getSupportFragmentManager().findFragmentById(R.id.fragementProfile);
        diveCenterFragment = (DiveCenterFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_dive_center);
        statusSertifikatFragment = (StatusSertifikatFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_status_sertifikat);
        pengaduanFragment = (PengaduanFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_pengaduan);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        filterDialogBerita = new FilterDialog();
        filterDialogReward = new FilterDialog();
        filterDialogEvent = new FilterDialog();

        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.hide(statusPembayaranFragment);
        fragmentTransaction.hide(registerPelatihanFragment);
        fragmentTransaction.hide(daftarMuridFragment);
        fragmentTransaction.hide(daftarPelatihanFragment);
        fragmentTransaction.hide(beritaFragment);
        fragmentTransaction.hide(profileFragment);
        fragmentTransaction.hide(diveCenterFragment);
        fragmentTransaction.hide(statusSertifikatFragment);
        fragmentTransaction.hide(pengaduanFragment);
        fragmentTransaction.show(beritaFragment);
        fragmentTransaction.commit();

    }


    @Override
    public void onSelectedSpinner(String text, int dialog_type) {
        beritaFragment.updateFilter(text, dialog_type);
    }
}
