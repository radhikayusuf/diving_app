package id.possi.possi.content.event;


import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import id.possi.possi.R;
import id.possi.possi.content.berita.BeritaFragment;
import id.possi.possi.dao.EventDao;
import id.possi.possi.dao.RewardDao;
import id.possi.possi.utils.LoadMoreCallback;

/**
 * A simple {@link Fragment} subclass.
 */
public class EventFragment extends Fragment implements NestedScrollView.OnScrollChangeListener, LoadMoreCallback {
    private RecyclerView recyclerView;
    private EventAdapter adapter;
    private NestedScrollView scrollView;
    private LinearLayoutManager layoutManager;
    private List<EventDao> mData = new ArrayList<>();
    private BeritaFragment evt;
    private List<EventDao> mDataOriginal = new ArrayList<>();

    public EventFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_event, container, false);
        initComponent(v);
        return v;
    }

    private void initComponent(View v) {
        recyclerView = (RecyclerView) v.findViewById(R.id.rc_event);
        scrollView = (NestedScrollView) v.findViewById(R.id.scroll_event);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        adapter = new EventAdapter(mData, this);
        recyclerView.setAdapter(adapter);

        scrollView.setOnScrollChangeListener(this);

        mData.add(new EventDao("1","Seminar POSSI Jakarta",getString(R.string.long_text), getString(R.string.dummy_long_text_event), "http://hellomakassar.com/wp-content/uploads/2016/12/kolam-renang-tirta-kartika.jpg" , "Ancol" , "Jakarta", "DKI Jakarta", 1493101710000L, -6.34, 107.843));
        mData.add(new EventDao("1","Workshop penyelaman",getString(R.string.long_text), getString(R.string.dummy_long_text_event), "http://cdn2.tstatic.net/kaltim/foto/bank/images/kolam-renang-di-rumah_20160423_190542.jpg", "Bikasoga" , "Buah Batu Bandung", "Jawa Barat", 1493101710000L, -6.34, 107.843));
        mData.add(new EventDao("1","Awarding kejuaraan",getString(R.string.long_text), getString(R.string.dummy_long_text_event), "http://hellobogor.com/wp-content/uploads/2015/02/wpid-photoeffect_14243667998141-1160x543.jpg", "Bikasoga" , "Buah Batu Bandung", "Jawa Barat", 1493101710000L, -6.34, 107.843));
        mData.add(new EventDao("1","Kumpul bersama anggota baru",getString(R.string.long_text), getString(R.string.dummy_long_text_event), "https://3.bp.blogspot.com/-twgKH9ZSUjk/VOvpHMqhSuI/AAAAAAAAB6M/MYRh-hX-l4I/s1600/Kolam+renang+Damai+Indah+Golf+BSD+www.poenyakoer.com+(4).JPG", "Bikasoga" , "Buah Batu Bandung", "Jawa Barat", 1493101710000L, -6.34, 107.843));

        mDataOriginal.addAll(mData);

        adapter.notifyDataSetChanged();
    }

    public void setBeritaInterface(BeritaFragment beritaInterface) {
        this.evt = beritaInterface;
    }

    @Override
    public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
        if(null != v && evt != null){
            if (scrollY < 20) {
                Log.wtf("onScrollChangeEvent: ", "true");
                evt.isOnTop(true);
            }else{
                evt.isOnTop(false);
                Log.wtf("onScrollChangeEvent: ", "false");
            }
        }
    }

    @Override
    public void onClickLoadmore(final ProgressBar progressBar, final TextView loadmore) {
        new Handler().postDelayed(new Runnable() {
            /**
             * setelah 3s
             */
            @Override
            public void run() {
            progressBar.setVisibility(View.GONE);
            loadmore.setVisibility(View.VISIBLE);
            loadMore();
            }
        }, 3000);
    }

    private void loadMore() {
        mData.add(new EventDao("1","Latihan Bersama",getString(R.string.long_text), getString(R.string.dummy_long_text_event), "https://3.bp.blogspot.com/-twgKH9ZSUjk/VOvpHMqhSuI/AAAAAAAAB6M/MYRh-hX-l4I/s1600/Kolam+renang+Damai+Indah+Golf+BSD+www.poenyakoer.com+(4).JPG", "Bikasoga" , "Buah Batu Bandung", "Jawa Barat", 1493101710000L, -6.34, 107.843));
        mData.add(new EventDao("1","Latihan Bersama",getString(R.string.long_text), getString(R.string.dummy_long_text_event), "https://3.bp.blogspot.com/-twgKH9ZSUjk/VOvpHMqhSuI/AAAAAAAAB6M/MYRh-hX-l4I/s1600/Kolam+renang+Damai+Indah+Golf+BSD+www.poenyakoer.com+(4).JPG", "Bikasoga" , "Buah Batu Bandung", "Jawa Barat", 1493101710000L, -6.34, 107.843));
        mData.add(new EventDao("1","Latihan Bersama",getString(R.string.long_text), getString(R.string.dummy_long_text_event), "https://3.bp.blogspot.com/-twgKH9ZSUjk/VOvpHMqhSuI/AAAAAAAAB6M/MYRh-hX-l4I/s1600/Kolam+renang+Damai+Indah+Golf+BSD+www.poenyakoer.com+(4).JPG", "Bikasoga" , "Buah Batu Bandung", "Jawa Barat", 1493101710000L, -6.34, 107.843));
        adapter.notifyDataSetChanged();
    }

    public void refreshBerita(final SwipeRefreshLayout swipeRefreshLayout) {
        mData.clear();

        mData.add(new EventDao("1","Awarding kejuaraan",getString(R.string.long_text), getString(R.string.dummy_long_text_event), "http://hellobogor.com/wp-content/uploads/2015/02/wpid-photoeffect_14243667998141-1160x543.jpg", "Bikasoga" , "Buah Batu Bandung", "Jawa Barat",1493101710000L, -6.34, 107.843));
        mData.add(new EventDao("1","Pelatihan Tahunan",getString(R.string.long_text), getString(R.string.dummy_long_text_event), "http://hellomakassar.com/wp-content/uploads/2016/12/kolam-renang-tirta-kartika.jpg" , "Bikasoga" , "Buah Batu Bandung",  "Jawa Barat",1493101710000L, -6.34, 107.843));
        mData.add(new EventDao("1","Latihan Serentak",getString(R.string.long_text), getString(R.string.dummy_long_text_event), "http://cdn2.tstatic.net/kaltim/foto/bank/images/kolam-renang-di-rumah_20160423_190542.jpg", "Bikasoga" , "Buah Batu Bandung", "Jawa Barat",1493101710000L, -6.34, 107.843));
        mData.add(new EventDao("1","Latihan",getString(R.string.long_text), getString(R.string.dummy_long_text_event), "https://3.bp.blogspot.com/-twgKH9ZSUjk/VOvpHMqhSuI/AAAAAAAAB6M/MYRh-hX-l4I/s1600/Kolam+renang+Damai+Indah+Golf+BSD+www.poenyakoer.com+(4).JPG", "Bikasoga" , "Buah Batu Bandung", "Jawa Barat", 1493101710000L, -6.34, 107.843));
        adapter.notifyDataSetChanged();

        new Handler().postDelayed(new Runnable() {
            /**
             * setelah 3s
             */
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
            }
        }, 3000);
    }

    public void updateRecycler(String provinsi) {
        List<EventDao> buffData = new ArrayList<>();

        if(!provinsi.equalsIgnoreCase("--")){
            for (EventDao eventDao : mData) {
                if(eventDao.getLokasi_provinsi().equalsIgnoreCase(provinsi))buffData.add(eventDao);
            }
            mData.clear();
            mData.addAll(buffData);
        }else{
            mData.clear();
            mData.addAll(mDataOriginal);
        }

        adapter.notifyDataSetChanged();
        }
}
