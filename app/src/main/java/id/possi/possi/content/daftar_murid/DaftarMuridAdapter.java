package id.possi.possi.content.daftar_murid;

import android.content.Context;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import id.possi.possi.R;
import id.possi.possi.dao.DaftarSiswaDao;
import id.possi.possi.utils.LoadMoreCallback;
import id.possi.possi.utils.MyDialog;

/**
 * Created by root on 11/04/17.
 */

public class DaftarMuridAdapter extends RecyclerView.Adapter<DaftarMuridAdapter.ViewHolder>{

    public static final int TYPE_MURID = 0;
    public static final int TYPE_LOAD_MORE = 1;

    private List<DaftarSiswaDao> mData = new ArrayList<>();
    private LoadMoreCallback callback;
    private MyDialog.CallbackDialogStatus evt;
    private Context mContext;

    public DaftarMuridAdapter(List<DaftarSiswaDao> mData, LoadMoreCallback callback, MyDialog.CallbackDialogStatus evt) {
        this.mData = mData;
        this.callback = callback;
        this.evt = evt;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        mContext = parent.getContext();
        return new ViewHolder(v, viewType == R.layout.daftar_siswa_row ? TYPE_MURID : TYPE_LOAD_MORE);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        if(holder.type == 0){
            holder.tvNama.setText(mData.get(position).getNama_lengkap());
            holder.tvEmail.setText(mData.get(position).getEmail());
            holder.tvMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    PopupMenu popup = new PopupMenu(mContext, holder.tvMenu);
                    popup.inflate(R.menu.daftar_siswa_menu);
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.change_stat_lunas:
                                    MyDialog.showNewGantiStatusDialog(mContext, mData.get(position).getNama_lengkap(), mData.get(position).isStatus_lunas(), evt, position);
                                    break;
                            }
                            return false;
                        }
                    });
                    popup.show();

                }
            });
            Picasso.with(mContext)
                    .load(mData.get(position).getImage_url())
                    .into(holder.image_profile);

            holder.v.setBackgroundResource(mData.get(position).isStatus_lunas() ?  R.drawable.circle_bg_tlulus : R.drawable.circle_bg_lulus);
        }else{
            holder.loadmore.setText("Muat Lainnya");

            holder.rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //callback.onClickLoadmore(holder.progressBar);
                }
            });
        }


    }


    @Override
    public int getItemViewType(int position) {
        if(position != mData.size()){
            return R.layout.daftar_siswa_row;
        }else{
            return R.layout.loadmore_nocard_row;
        }
    }

    @Override
    public int getItemCount() {
        return mData != null ? mData.size() + 1 : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvNama, tvEmail, tvMenu, loadmore;
        public ImageView image_profile;
        public View v, rootView;
        public int type = 0;

        public ViewHolder(View v, int i) {
            super(v);
            this.rootView = v;
            this.type = i;
            if(i == TYPE_MURID){
                this.v = v.findViewById(R.id.indicator_lulus);
                tvNama = (TextView)v.findViewById(R.id.siswa_name);
                tvEmail = (TextView)v.findViewById(R.id.siswa_email);
                tvMenu = (TextView)v.findViewById(R.id.siswa_option);
                image_profile = (ImageView)v.findViewById(R.id.siswa_pic);
            }else{
                loadmore = (TextView) v.findViewById(R.id.load_more);
            }
        }
    }



}
