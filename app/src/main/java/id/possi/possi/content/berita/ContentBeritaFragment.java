package id.possi.possi.content.berita;


import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import id.possi.possi.R;
import id.possi.possi.dao.BeritaDao;
import id.possi.possi.utils.Helper;
import id.possi.possi.utils.LoadMoreCallback;

/**
 * A simple {@link Fragment} subclass.
 */
public class ContentBeritaFragment extends Fragment implements LoadMoreCallback, NestedScrollView.OnScrollChangeListener {

    private RecyclerView rc_berita;
    private List<BeritaDao> mData = new ArrayList<>();
    private List<BeritaDao> mDataOriginal = new ArrayList<>();
    private BeritaAdapter adapter;
    private LinearLayoutManager layoutManager;
    private NestedScrollView nestedScrollView;
    private BeritaInterface evt;
    private View v;

    public ContentBeritaFragment() {
        // Required empty public constructor
    }

//    public ContentBeritaFragment(BeritaInterface evt) {
//        this.evt = evt;
//    }

    public void setBeritaInterface(BeritaInterface evt){
        this.evt = evt;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_content_berita, container, false);
        initComponent(v);
        return v;
    }

    private void initComponent(View v) {
        rc_berita = (RecyclerView) v.findViewById(R.id.rc_berita);
        nestedScrollView = (NestedScrollView) v.findViewById(R.id.nested_berita);
        nestedScrollView.setOnScrollChangeListener(this);
        adapter = new BeritaAdapter(mData, this);
        layoutManager = new LinearLayoutManager(getContext());
        rc_berita.setAdapter(adapter);
        rc_berita.setLayoutManager(layoutManager);
        rc_berita.setNestedScrollingEnabled(false);
        //Data Dummy
        mData.add(new BeritaDao(1, 1493101710000L, "Admin", "Pemenang PON Selam Jabar", "https://cdns.klimg.com/merdeka.com/i/w/news/2016/09/16/754975/540x270/selam-jatim-panen-empat-emas-di-pon-2016-20160916213510.jpg", "Kontingen Jawa Timur menunjukkan keperkasaannya di cabang olahraga selam laut Pekan OLahraga Nasional (PON) XIX/2016 Jawa Barat yang digelar di Pantai Tirtamaya Kabupaten Indramayu. Jatim berhasil menjadi juara umum setelah mendominasi pengumpulan medali. \n" +
                "Kontingen Jatim secara keseluruhan berhasil meraih 2 emas, 2 perak, dan 1 perunggu. Di urutan kedua menguntit kontingen DKI Jakarta dengan 2 emas, 1 perak, 1 perunggu. Sementara urutan ketiga adalah Papua dengan 1 emas 1 perak. \n" +
                "\n" +
                "Pada hari terakhir pertandingan selam laut, Kamis (22/9), Jawa Timur menambah 1 medali emas melalui Rodrick Luhur, yang turun di nomor bergengsi fins swimming 10.000 meter putra. \n" +
                "\n" +
                "Di posisi kedua ditempati peselam DKI Jakarta, Andi Fabian yang berhak atas medali perak, dengan catatan waktu 02:01:33,97 detik. Sedangkan medali perunggu diraih peselam Sulawesi Utara, Andrew Rorimpandey dengan waktu 02:02:06,10 detik. " +
                "Pemenang PON Selam Jabar\", \"https://cdns.klimg.com/merdeka.com/i/w/news/2016/09/16/754975/540x270/selam-jatim-panen-empat-emas-di-pon-2016-20160916213510.jpg\", \"Kontingen Jawa Timur menunjukkan keperkasaannya di cabang olahraga selam laut Pekan OLahraga Nasional (PON) XIX/2016 Jawa Barat yang digelar di Pantai Tirtamaya Kabupaten Indramayu. Jatim berhasil menjadi juara umum setelah mendominasi pengumpulan medali. \\n\" +\n" +
                "                \"Kontingen Jatim secara keseluruhan berhasil meraih 2 emas, 2 perak, dan 1 perunggu. Di urutan kedua menguntit kontingen DKI Jakarta dengan 2 emas, 1 perak, 1 perunggu. Sementara urutan ketiga adalah Papua dengan 1 emas 1 perak. \\n\" +\n" +
                "                \"\\n\" +\n" +
                "                \"Pada hari terakhir pertandingan selam laut, Kamis (22/9), Jawa Timur menambah 1 medali emas melalui Rodrick Luhur, yang turun di nomor bergengsi fins swimming 10.000 meter putra. \\n\" +\n" +
                "                \"\\n\" +\n" +
                "                \"Di posisi kedua ditempati peselam DKI Jakarta, Andi Fabian yang berhak atas medali perak, dengan catatan waktu 02:01:33,97 detik. Sedangkan medali perunggu diraih peselam Sulawesi Utara, Andrew Rorimpandey dengan waktu 02:02:06,10 detik.", "Jawa Barat"));
        mData.add(new BeritaDao(2, 1493101711, "Admin" ," BALI Standar Praktek Penyelaman yang Aman", "https://scubamania.files.wordpress.com/2010/03/pool-practice.jpg", "Berikut  ini adalah hasil kompilasi tata cara praktek menyelam dengan tujuan meningkatkan rasa aman dan nyaman dalam melakukan Scuba diving sesuai prosedur yang telah diajarkan pada pendidikan selam awal.\n" +
                "\n" +
                "Memelihara kesehatan fisik dan mental yang baik untuk menyelam. Menghindari pengaruh alkohol atau obat berbahaya sewaktu melakukan aktifitas penyelaman.\n" +
                "\n" +
                "\n" +
                "Tetap melatih keterampilan Scuba Diving, selalu berusaha untuk meningkatkan keterampilan melalui pelatihan berkelanjutan, dan meninjau ulang pada kondisi terkendali setelah sebuah periode tertentu tidak aktif menyelam, dan merujuk pada material pelatihan untuk tetap ingat pada informasi yang penting.", "Bali"));
        mData.add(new BeritaDao(3, 1493101711 * 1000, "Admin","POSSI - Persatuan Olahraga Selam Seluruh Indonesia", "http://kanalsatu.com/images/20170302-211232_40.jpg", "Olahraga Selam adalah jenis atau cabang olahraga yang istimewa, karena olahraga ini memiliki muatan yang dapat dikembangkan kearah prestasi, rekreasi maupun profesi. Olahraga selam telah ada di Indonesia sebelum tahun 1962 tetapi kebanyakan dilakukan oleh orang asing yang bekerja di Indonesia, pada tahun 1962 TNI-AL mendirikan Instalasi Pusat Penyelaman dan Sekolah Penyelaman. Dengan berdirinya kedua lembaga tersebut maka makin bertambah banyak orang Indonesia yang berlatih dan belajar selam, terutama di lingkungan TNI-AL.", "Jawa Barat"));
        mData.add(new BeritaDao(3, 1493101711 * 1000, "Admin","POSSI - Persatuan Olahraga Selam Seluruh Indonesia", "http://kanalsatu.com/images/20170302-211232_40.jpg", "Olahraga Selam adalah jenis atau cabang olahraga yang istimewa, karena olahraga ini memiliki muatan yang dapat dikembangkan kearah prestasi, rekreasi maupun profesi. Olahraga selam telah ada di Indonesia sebelum tahun 1962 tetapi kebanyakan dilakukan oleh orang asing yang bekerja di Indonesia, pada tahun 1962 TNI-AL mendirikan Instalasi Pusat Penyelaman dan Sekolah Penyelaman. Dengan berdirinya kedua lembaga tersebut maka makin bertambah banyak orang Indonesia yang berlatih dan belajar selam, terutama di lingkungan TNI-AL.", "Jawa Barat"));
        mData.add(new BeritaDao(3, 1493124960, "Admin","POSSI - Persatuan Olahraga Selam Seluruh Indonesia", "http://kanalsatu.com/images/20170302-211232_40.jpg", "Olahraga Selam adalah jenis atau cabang olahraga yang istimewa, karena olahraga ini memiliki muatan yang dapat dikembangkan kearah prestasi, rekreasi maupun profesi. Olahraga selam telah ada di Indonesia sebelum tahun 1962 tetapi kebanyakan dilakukan oleh orang asing yang bekerja di Indonesia, pada tahun 1962 TNI-AL mendirikan Instalasi Pusat Penyelaman dan Sekolah Penyelaman. Dengan berdirinya kedua lembaga tersebut maka makin bertambah banyak orang Indonesia yang berlatih dan belajar selam, terutama di lingkungan TNI-AL.", "Jawa Tengah"));
        mData.add(new BeritaDao(3, 1493101711, "Admin","POSSI - Persatuan Olahraga Selam Seluruh Indonesia", "http://kanalsatu.com/images/20170302-211232_40.jpg", "Olahraga Selam adalah jenis atau cabang olahraga yang istimewa, karena olahraga ini memiliki muatan yang dapat dikembangkan kearah prestasi, rekreasi maupun profesi. Olahraga selam telah ada di Indonesia sebelum tahun 1962 tetapi kebanyakan dilakukan oleh orang asing yang bekerja di Indonesia, pada tahun 1962 TNI-AL mendirikan Instalasi Pusat Penyelaman dan Sekolah Penyelaman. Dengan berdirinya kedua lembaga tersebut maka makin bertambah banyak orang Indonesia yang berlatih dan belajar selam, terutama di lingkungan TNI-AL.", "DKI Jakarta"));
        mData.add(new BeritaDao(3, 1493124960, "Admin","BALI POSSI - Persatuan Olahraga Selam Seluruh Indonesia", "http://kanalsatu.com/images/20170302-211232_40.jpg", "Olahraga Selam adalah jenis atau cabang olahraga yang istimewa, karena olahraga ini memiliki muatan yang dapat dikembangkan kearah prestasi, rekreasi maupun profesi. Olahraga selam telah ada di Indonesia sebelum tahun 1962 tetapi kebanyakan dilakukan oleh orang asing yang bekerja di Indonesia, pada tahun 1962 TNI-AL mendirikan Instalasi Pusat Penyelaman dan Sekolah Penyelaman. Dengan berdirinya kedua lembaga tersebut maka makin bertambah banyak orang Indonesia yang berlatih dan belajar selam, terutama di lingkungan TNI-AL.", "Bali"));
        adapter.notifyDataSetChanged();

        mDataOriginal.addAll(mData);

    }

    public void refreshBerita(final SwipeRefreshLayout layout){
        mData.clear();
        //Data Dummy
        mData.add(new BeritaDao(1, 1493101710000L, "Admin", "Pemenang PON Selam Jabar", "https://cdns.klimg.com/merdeka.com/i/w/news/2016/09/16/754975/540x270/selam-jatim-panen-empat-emas-di-pon-2016-20160916213510.jpg", "Kontingen Jawa Timur menunjukkan keperkasaannya di cabang olahraga selam laut Pekan OLahraga Nasional (PON) XIX/2016 Jawa Barat yang digelar di Pantai Tirtamaya Kabupaten Indramayu. Jatim berhasil menjadi juara umum setelah mendominasi pengumpulan medali. \n" +
                "Kontingen Jatim secara keseluruhan berhasil meraih 2 emas, 2 perak, dan 1 perunggu. Di urutan kedua menguntit kontingen DKI Jakarta dengan 2 emas, 1 perak, 1 perunggu. Sementara urutan ketiga adalah Papua dengan 1 emas 1 perak. \n" +
                "\n" +
                "Pada hari terakhir pertandingan selam laut, Kamis (22/9), Jawa Timur menambah 1 medali emas melalui Rodrick Luhur, yang turun di nomor bergengsi fins swimming 10.000 meter putra. \n" +
                "\n" +
                "Di posisi kedua ditempati peselam DKI Jakarta, Andi Fabian yang berhak atas medali perak, dengan catatan waktu 02:01:33,97 detik. Sedangkan medali perunggu diraih peselam Sulawesi Utara, Andrew Rorimpandey dengan waktu 02:02:06,10 detik. " +
                "Pemenang PON Selam Jabar\", \"https://cdns.klimg.com/merdeka.com/i/w/news/2016/09/16/754975/540x270/selam-jatim-panen-empat-emas-di-pon-2016-20160916213510.jpg\", \"Kontingen Jawa Timur menunjukkan keperkasaannya di cabang olahraga selam laut Pekan OLahraga Nasional (PON) XIX/2016 Jawa Barat yang digelar di Pantai Tirtamaya Kabupaten Indramayu. Jatim berhasil menjadi juara umum setelah mendominasi pengumpulan medali. \\n\" +\n" +
                "                \"Kontingen Jatim secara keseluruhan berhasil meraih 2 emas, 2 perak, dan 1 perunggu. Di urutan kedua menguntit kontingen DKI Jakarta dengan 2 emas, 1 perak, 1 perunggu. Sementara urutan ketiga adalah Papua dengan 1 emas 1 perak. \\n\" +\n" +
                "                \"\\n\" +\n" +
                "                \"Pada hari terakhir pertandingan selam laut, Kamis (22/9), Jawa Timur menambah 1 medali emas melalui Rodrick Luhur, yang turun di nomor bergengsi fins swimming 10.000 meter putra. \\n\" +\n" +
                "                \"\\n\" +\n" +
                "                \"Di posisi kedua ditempati peselam DKI Jakarta, Andi Fabian yang berhak atas medali perak, dengan catatan waktu 02:01:33,97 detik. Sedangkan medali perunggu diraih peselam Sulawesi Utara, Andrew Rorimpandey dengan waktu 02:02:06,10 detik.","DKI Jakarta"));
        mData.add(new BeritaDao(2, 1493101711, "Admin" ,"Standar Praktek Penyelaman yang Aman", "https://scubamania.files.wordpress.com/2010/03/pool-practice.jpg", "Berikut  ini adalah hasil kompilasi tata cara praktek menyelam dengan tujuan meningkatkan rasa aman dan nyaman dalam melakukan Scuba diving sesuai prosedur yang telah diajarkan pada pendidikan selam awal.\n" +
                "\n" +
                "Memelihara kesehatan fisik dan mental yang baik untuk menyelam. Menghindari pengaruh alkohol atau obat berbahaya sewaktu melakukan aktifitas penyelaman.\n" +
                "\n" +
                "\n" +
                "Tetap melatih keterampilan Scuba Diving, selalu berusaha untuk meningkatkan keterampilan melalui pelatihan berkelanjutan, dan meninjau ulang pada kondisi terkendali setelah sebuah periode tertentu tidak aktif menyelam, dan merujuk pada material pelatihan untuk tetap ingat pada informasi yang penting.", "DKI Jakarta"));
        mData.add(new BeritaDao(3, 1493101711 * 1000, "Admin","POSSI - Persatuan Olahraga Selam Seluruh Indonesia", "http://kanalsatu.com/images/20170302-211232_40.jpg", "Olahraga Selam adalah jenis atau cabang olahraga yang istimewa, karena olahraga ini memiliki muatan yang dapat dikembangkan kearah prestasi, rekreasi maupun profesi. Olahraga selam telah ada di Indonesia sebelum tahun 1962 tetapi kebanyakan dilakukan oleh orang asing yang bekerja di Indonesia, pada tahun 1962 TNI-AL mendirikan Instalasi Pusat Penyelaman dan Sekolah Penyelaman. Dengan berdirinya kedua lembaga tersebut maka makin bertambah banyak orang Indonesia yang berlatih dan belajar selam, terutama di lingkungan TNI-AL.", "DKI Jakarta"));
        mData.add(new BeritaDao(3, 1493101711 * 1000, "Admin","POSSI - Persatuan Olahraga Selam Seluruh Indonesia", "http://kanalsatu.com/images/20170302-211232_40.jpg", "Olahraga Selam adalah jenis atau cabang olahraga yang istimewa, karena olahraga ini memiliki muatan yang dapat dikembangkan kearah prestasi, rekreasi maupun profesi. Olahraga selam telah ada di Indonesia sebelum tahun 1962 tetapi kebanyakan dilakukan oleh orang asing yang bekerja di Indonesia, pada tahun 1962 TNI-AL mendirikan Instalasi Pusat Penyelaman dan Sekolah Penyelaman. Dengan berdirinya kedua lembaga tersebut maka makin bertambah banyak orang Indonesia yang berlatih dan belajar selam, terutama di lingkungan TNI-AL.", "Jawa Barat"));
        mData.add(new BeritaDao(3, 1493124960, "Admin","POSSI - Persatuan Olahraga Selam Seluruh Indonesia", "http://kanalsatu.com/images/20170302-211232_40.jpg", "Olahraga Selam adalah jenis atau cabang olahraga yang istimewa, karena olahraga ini memiliki muatan yang dapat dikembangkan kearah prestasi, rekreasi maupun profesi. Olahraga selam telah ada di Indonesia sebelum tahun 1962 tetapi kebanyakan dilakukan oleh orang asing yang bekerja di Indonesia, pada tahun 1962 TNI-AL mendirikan Instalasi Pusat Penyelaman dan Sekolah Penyelaman. Dengan berdirinya kedua lembaga tersebut maka makin bertambah banyak orang Indonesia yang berlatih dan belajar selam, terutama di lingkungan TNI-AL.", "Jawa Timur"));
        mData.add(new BeritaDao(3, 1493101711, "Admin","BALI POSSI - Persatuan Olahraga Selam Seluruh Indonesia", "http://kanalsatu.com/images/20170302-211232_40.jpg", "Olahraga Selam adalah jenis atau cabang olahraga yang istimewa, karena olahraga ini memiliki muatan yang dapat dikembangkan kearah prestasi, rekreasi maupun profesi. Olahraga selam telah ada di Indonesia sebelum tahun 1962 tetapi kebanyakan dilakukan oleh orang asing yang bekerja di Indonesia, pada tahun 1962 TNI-AL mendirikan Instalasi Pusat Penyelaman dan Sekolah Penyelaman. Dengan berdirinya kedua lembaga tersebut maka makin bertambah banyak orang Indonesia yang berlatih dan belajar selam, terutama di lingkungan TNI-AL.", "Bali"));
        mData.add(new BeritaDao(3, 1493124960, "Admin","POSSI - Persatuan Olahraga Selam Seluruh Indonesia", "http://kanalsatu.com/images/20170302-211232_40.jpg", "Olahraga Selam adalah jenis atau cabang olahraga yang istimewa, karena olahraga ini memiliki muatan yang dapat dikembangkan kearah prestasi, rekreasi maupun profesi. Olahraga selam telah ada di Indonesia sebelum tahun 1962 tetapi kebanyakan dilakukan oleh orang asing yang bekerja di Indonesia, pada tahun 1962 TNI-AL mendirikan Instalasi Pusat Penyelaman dan Sekolah Penyelaman. Dengan berdirinya kedua lembaga tersebut maka makin bertambah banyak orang Indonesia yang berlatih dan belajar selam, terutama di lingkungan TNI-AL.", "Jawa Barat"));
        adapter.notifyDataSetChanged();
        new Handler().postDelayed(new Runnable() {
            /**
             * setelah 3s
             */
            @Override
            public void run() {
                layout.setRefreshing(false);
            }
        }, 3000);
    }

    public void loadMore(){
        mData.add(new BeritaDao(1, 1493101710000L, "Admin", "Pemenang PON Selam Jabar", "https://cdns.klimg.com/merdeka.com/i/w/news/2016/09/16/754975/540x270/selam-jatim-panen-empat-emas-di-pon-2016-20160916213510.jpg", "Kontingen Jawa Timur menunjukkan keperkasaannya di cabang olahraga selam laut Pekan OLahraga Nasional (PON) XIX/2016 Jawa Barat yang digelar di Pantai Tirtamaya Kabupaten Indramayu. Jatim berhasil menjadi juara umum setelah mendominasi pengumpulan medali. \n" +
                "Kontingen Jatim secara keseluruhan berhasil meraih 2 emas, 2 perak, dan 1 perunggu. Di urutan kedua menguntit kontingen DKI Jakarta dengan 2 emas, 1 perak, 1 perunggu. Sementara urutan ketiga adalah Papua dengan 1 emas 1 perak. \n" +
                "\n" +
                "Pada hari terakhir pertandingan selam laut, Kamis (22/9), Jawa Timur menambah 1 medali emas melalui Rodrick Luhur, yang turun di nomor bergengsi fins swimming 10.000 meter putra. \n" +
                "\n" +
                "Di posisi kedua ditempati peselam DKI Jakarta, Andi Fabian yang berhak atas medali perak, dengan catatan waktu 02:01:33,97 detik. Sedangkan medali perunggu diraih peselam Sulawesi Utara, Andrew Rorimpandey dengan waktu 02:02:06,10 detik. " +
                "Pemenang PON Selam Jabar\", \"https://cdns.klimg.com/merdeka.com/i/w/news/2016/09/16/754975/540x270/selam-jatim-panen-empat-emas-di-pon-2016-20160916213510.jpg\", \"Kontingen Jawa Timur menunjukkan keperkasaannya di cabang olahraga selam laut Pekan OLahraga Nasional (PON) XIX/2016 Jawa Barat yang digelar di Pantai Tirtamaya Kabupaten Indramayu. Jatim berhasil menjadi juara umum setelah mendominasi pengumpulan medali. \\n\" +\n" +
                "                \"Kontingen Jatim secara keseluruhan berhasil meraih 2 emas, 2 perak, dan 1 perunggu. Di urutan kedua menguntit kontingen DKI Jakarta dengan 2 emas, 1 perak, 1 perunggu. Sementara urutan ketiga adalah Papua dengan 1 emas 1 perak. \\n\" +\n" +
                "                \"\\n\" +\n" +
                "                \"Pada hari terakhir pertandingan selam laut, Kamis (22/9), Jawa Timur menambah 1 medali emas melalui Rodrick Luhur, yang turun di nomor bergengsi fins swimming 10.000 meter putra. \\n\" +\n" +
                "                \"\\n\" +\n" +
                "                \"Di posisi kedua ditempati peselam DKI Jakarta, Andi Fabian yang berhak atas medali perak, dengan catatan waktu 02:01:33,97 detik. Sedangkan medali perunggu diraih peselam Sulawesi Utara, Andrew Rorimpandey dengan waktu 02:02:06,10 detik.", "Jawa Barat"));

        adapter.notifyDataSetChanged();
    }

    @Override
    public void onClickLoadmore(final ProgressBar progressBar, final TextView loadmore) {
        new Handler().postDelayed(new Runnable() {
            /**
             * setelah 3s
             */
            @Override
            public void run() {
                loadMore();
                progressBar.setVisibility(View.GONE);
                loadmore.setVisibility(View.VISIBLE);
            }
        }, 3000);

    }

    @Override
    public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
        if(null != v && evt != null){
            if (scrollY < 20) {
                Log.wtf("onScrollChange: ", "true");
                evt.isOnTop(true);
            }else{
                evt.isOnTop(false);
                Log.wtf("onScrollChange: ", "false");
            }
        }
    }

    public void updateRecycler(String provinsi) {
        if(!provinsi.equalsIgnoreCase("--")){
            List<BeritaDao> buff_data = new ArrayList<>();
            for (BeritaDao beritaDao : mData) {
                if(beritaDao.getLokasi_provinsi().equalsIgnoreCase(provinsi)){
                    buff_data.add(beritaDao);
                }
            }
            mData.clear();
            mData.addAll(buff_data);
        }else{
            mData.clear();
            mData.addAll(mDataOriginal);
        }

        adapter.notifyDataSetChanged();

    }
}
