package id.possi.possi.content.edit_profile;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import de.hdodenhof.circleimageview.CircleImageView;
import id.possi.possi.R;
import id.possi.possi.content.biodata.InputDataActivity;
import id.possi.possi.content.edit_password.EditPasswordActivity;
import id.possi.possi.dao.ProfileDao;
import id.possi.possi.utils.Helper;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.RuntimePermissions;


@RuntimePermissions
public class EditProfileActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = EditProfileActivity.class.getSimpleName();
    private ScrollView scrollView;
    private CircleImageView image_profile;
    private TextInputEditText nama_lengkap, email, phone, tanggal_lahir, tempat_lahir, provinsi, kota, no_ktp;
    private EditText alamat;
    private TextInputLayout inputlayout_tanggal_lahir, inputlayout_provinsi;
    private TextView tv_date, tv_provinsi, tv_editfoto;
    private ProfileDao mData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        initComponent();
        inputDataToView();
    }

    private void initComponent() {
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setTitle("Edit Profile");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        image_profile = (CircleImageView)findViewById(R.id.image_edit_profile);

        scrollView = (ScrollView)findViewById(R.id.scroll_edit_profile);

        nama_lengkap = (TextInputEditText) findViewById(R.id.edit_nama_lengkap);
        email = (TextInputEditText) findViewById(R.id.edit_email);
        phone = (TextInputEditText) findViewById(R.id.edit_phone);
        tanggal_lahir = (TextInputEditText) findViewById(R.id.edit_tanggal_lahir);
        tempat_lahir = (TextInputEditText) findViewById(R.id.edit_tempat_lahir);
        kota = (TextInputEditText) findViewById(R.id.edit_kota);
        alamat = (EditText) findViewById(R.id.edit_alamat);
        provinsi = (TextInputEditText) findViewById(R.id.edit_provinsi);
        no_ktp = (TextInputEditText) findViewById(R.id.edit_no_ktp);


        inputlayout_tanggal_lahir = (TextInputLayout) findViewById(R.id.tanggal_lahir);
        inputlayout_provinsi = (TextInputLayout) findViewById(R.id.provinsi);

        tv_editfoto = (TextView) findViewById(R.id.tv_edit_foto);
        tv_date = (TextView) findViewById(R.id.tv_date);
        tv_provinsi = (TextView) findViewById(R.id.tv_provinsi);

        inputlayout_tanggal_lahir.setOnClickListener(this);
        tanggal_lahir.setOnClickListener(this);
        inputlayout_provinsi.setOnClickListener(this);
        provinsi.setOnClickListener(this);
        tv_date.setOnClickListener(this);
        tv_provinsi.setOnClickListener(this);
        tv_editfoto.setOnClickListener(this);
        image_profile.setOnClickListener(this);
    }


    private void inputDataToView() {
        mData = new ProfileDao("Edward", "edward@gmail.com","qwe123qwe123","Jawa Barat", "1113333", getString(R.string.long_text), "083126379234","Jakarta","cbxvhbxsa", "Bandung", "http://bootstrap.gallery/everest-v3/img/user4.jpg", "796527692827",1421312,321412,true, 610964390000L);

        nama_lengkap.setText(mData.getName());
        email.setText(mData.getEmail());
        tanggal_lahir.setText(Helper.longToStringCustomDateFormat(mData.getTanggal_lahir(), "dd/MM/yyyy"));
        tempat_lahir.setText(mData.getTempat_lahir());
        phone.setText(mData.getPhone());
        kota.setText(mData.getKota());
        phone.setText(mData.getPhone());
        alamat.setText(mData.getAlamat());
        provinsi.setText(mData.getProvinsi());
        no_ktp.setText(mData.getNo_ktp());

        Picasso.with(this)
                .load(mData.getImage_url())
                .into(image_profile);

        scrollView.smoothScrollTo(0,image_profile.getTop());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.save_edit:
                Toast.makeText(this, "Save", Toast.LENGTH_SHORT).show();
                break;
            case R.id.save_edit_password:
                startActivity(new Intent(this, EditPasswordActivity.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.save_edit_profile, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public static void startActivity(Context context) {
        context.startActivity(new Intent(context, EditProfileActivity.class));
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.tanggal_lahir || (view.getId() == R.id.edit_tanggal_lahir || view.getId() == R.id.tv_date)){
            String date[] = tanggal_lahir.getText().toString().split("/");


            DatePickerDialog datePicker = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                    tanggal_lahir.setText(String.valueOf((day < 10 ? "0"+day : day)+"/"+((month+1 < 10 ? "0"+(month+1) : ""+(month+1))+"/"+year)));
                }
            },  Integer.parseInt(date[2]), Integer.parseInt(date[1])-1 ,Integer.parseInt(date[0]));
            datePicker.show();
        }else if(view.getId() == R.id.tv_edit_foto || view.getId() == R.id.image_edit_profile){
            EditProfileDispatcher.doStartCameraWithCheck(this);
        }else{
            showDialogProvisi();
        }
    }

    void showDialogProvisi(){
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(EditProfileActivity.this);
        builderSingle.setTitle("Pilih Provinsi");

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(EditProfileActivity.this, android.R.layout.select_dialog_singlechoice);
        arrayAdapter.add("Jawa Barat");
        arrayAdapter.add("Jawa Tengah");
        arrayAdapter.add("Jawa Timur");


        builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String strName = arrayAdapter.getItem(which);
                provinsi.setText(strName);
//                    AlertDialog.Builder builderInner = new AlertDialog.Builder(InputDataActivity.this);
//                    builderInner.setMessage(strName);
//                    builderInner.setTitle("Your Selected Item is");
//                    builderInner.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog,int which) {
//                            dialog.dismiss();
//                        }
//                    });
//                    builderInner.show();
            }
        });
        builderSingle.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        EditProfileDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    @Override
    @SuppressWarnings("NewApi")
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE) {

            Log.d(TAG, "PICK_IMAGE_CHOOSER_REQUEST_CODE");

            if (resultCode == RESULT_OK){

                Log.d(TAG, "RESULT_OK");

                Uri imageUri = CropImage.getPickImageResultUri(this, data);
                if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                    Log.d(TAG, "read external storage permission required");
                    //WorkOrderCreateActivityPermissionsDispatcher.doStartPickImageWithCheck(this);
                } else {
                    Log.d(TAG, "read external storage permission not required");
                    CropImage.activity(imageUri)
                            .setGuidelines(CropImageView.Guidelines.ON)
                            .start(this);
                }
            } else {
                Log.d(TAG, String.valueOf(resultCode));
            }
        }
        else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            final CropImage.ActivityResult result = CropImage.getActivityResult(data);

            Log.d(TAG, "CROP_IMAGE_ACTIVITY_REQUEST_CODE");

            if (resultCode == RESULT_OK) {
                final ProgressDialog dialog = ProgressDialog.show(this, null, "Loading");
                //Bitmap selectedBitmap = BitmapFactory.decodeFile(result.getUri().getPath());
                //                Bitmap bitmap = ImageHelper.decodeSampledBitmapFromResource(result.getUri().getPath(), 100, 100);

                //byte[] webpImageData = WebPFactory.nativeEncodeBitmap(selectedBitmap, 80);
                //Bitmap bitmap = WebPFactory.nativeDecodeByteArray(webpImageData, null);

                //                mViewModel.setImageBitmap(bitmap);
                image_profile.setImageBitmap(Helper.decodeSampledBitmapFromResource(result.getUri().getPath()));
                dialog.dismiss();

                //                Observable.fromCallable(() -> ImageHelper.convertToWebP(this, result.getUri().getPath()))
                //                        .subscribeOn(Schedulers.newThread())
                //                        .observeOn(AndroidSchedulers.mainThread())
                //                        .subscribe(new Observer<Bitmap>() {
                //                            @Override
                //                            public void onCompleted() {
                //                                dialog.dismiss();
                //                            }
                //
                //                            @Override
                //                            public void onError(Throwable e) {
                //                                Toast.makeText(WorkOrderCreateActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                //                            }
                //
                //                            @Override
                //                            public void onNext(Bitmap bitmap) {
                //                                mViewModel.setImageBitmap(bitmap);
                //                            }
                //                        });
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Log.d(TAG, "CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE");

                Exception error = result.getError();
                Log.d(TAG, error.getMessage());
            }
        }
        else {
            Log.d(TAG, String.valueOf(requestCode));}

    }

    @OnShowRationale(Manifest.permission.CAMERA)
    void showRationaleForCamera(final permissions.dispatcher.PermissionRequest request) {
        new AlertDialog.Builder(this)
                .setMessage("You cannot load image from your external storage unless you allow read external storage permission")
                .setPositiveButton(R.string.button_allow, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        request.proceed();
                    }
                })
                .setNegativeButton(R.string.button_deny, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        request.cancel();
                    }
                })
                .show();
    }

    @OnPermissionDenied(Manifest.permission.READ_EXTERNAL_STORAGE)
    void showDeniedForReadExternalStorage() {
        Toast.makeText(this, R.string.permission_external_storage_denied, Toast.LENGTH_LONG).show();
    }

    @OnNeverAskAgain(Manifest.permission.CAMERA)
    void showNeverAskForCamera() {
        Toast.makeText(this, "Permission Denied", Toast.LENGTH_LONG).show();
    }

    @NeedsPermission(Manifest.permission.CAMERA)
    public void doStartCamera() {
        CropImage.startPickImageActivity(this);
    }

    @NeedsPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
    public void doStartPickImage() {
        CropImage.startPickImageActivity(this);
    }
}
