package id.possi.possi.content.status_pembayaran;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import id.possi.possi.R;
import id.possi.possi.dao.StatusPembayaranDao;
import id.possi.possi.utils.LoadMoreCallback;

/**
 * Created by root on 12/04/17.
 */

public class StatusPembayaranAdapter extends RecyclerView.Adapter<StatusPembayaranAdapter.ViewHolder> {

    public static final int TYPE_PEMBAYARAN = 0;
    public static final int TYPE_LOAD_MORE = 1;

    private List<StatusPembayaranDao> mData = new ArrayList<>();
    private LoadMoreCallback evt;

    public StatusPembayaranAdapter(List<StatusPembayaranDao> mData, LoadMoreCallback evt) {
        this.mData = mData;
        this.evt = evt;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        return new ViewHolder(v, viewType == R.layout.row_pembayaran ? TYPE_PEMBAYARAN : TYPE_LOAD_MORE);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        if(holder.type == TYPE_PEMBAYARAN){
            NumberFormat rupiahFormat = NumberFormat.getInstance(Locale.GERMANY);
            String rupiah = rupiahFormat.format(Double.parseDouble(String.valueOf(mData.get(position).getTotal_pelatihan())));

            holder.title.setText(mData.get(position).getNama_pelatihan());
            holder.total.setText("Jumlah : Rp."+rupiah);
            holder.status.setText("Status : " + (mData.get(position).isLunas() ? "Lunas" : "Belum"));
        }else{
            holder.rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holder.loadmore.setText("Muat Lainnya");
                    holder.loadmore.setVisibility(View.GONE);
                    holder.progressBar.setVisibility(View.VISIBLE);
                    evt.onClickLoadmore(holder.progressBar, holder.loadmore);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return mData != null ? mData.size() + 1 : 0;
    }

    @Override
    public int getItemViewType(int position) {
        if(position != mData.size()){
            return R.layout.row_pembayaran;
        }else{
            return R.layout.loadmore_nocard_row;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView title, status, total, loadmore;
        int type = TYPE_PEMBAYARAN;
        ProgressBar progressBar;
        View rootView;


        public ViewHolder(View v, int type) {
            super(v);
            rootView = v;
            this.type = type;

            if(type == TYPE_PEMBAYARAN){
                title = (TextView)v.findViewById(R.id.title_pembayaran);
                status = (TextView)v.findViewById(R.id.txt_status);
                total = (TextView) v.findViewById(R.id.txt_total);
            }else{
                loadmore = (TextView)v.findViewById(R.id.load_more);
                progressBar = (ProgressBar) v.findViewById(R.id.progress_bar);
            }
        }
    }
}
