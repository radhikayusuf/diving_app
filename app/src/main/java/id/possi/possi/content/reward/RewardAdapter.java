package id.possi.possi.content.reward;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import id.possi.possi.R;
import id.possi.possi.content.detail_reward.DetailRewardActivity;
import id.possi.possi.dao.RewardDao;
import id.possi.possi.utils.Helper;
import id.possi.possi.utils.LoadMoreCallback;

/**
 * Created by root on 12/05/17.
 */

public class RewardAdapter extends RecyclerView.Adapter<RewardAdapter.ViewHolder> {

    public static final int TYPE_REWARD = 0;
    public static final int TYPE_LOAD_MORE = 1;
    private List<RewardDao> mData = new ArrayList<>();
    private Context mContext;
    private LoadMoreCallback callback;

    public RewardAdapter(List<RewardDao> mData, LoadMoreCallback callback) {
        this.mData = mData;
        this.callback = callback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View v = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        return new ViewHolder(v, viewType == R.layout.row_reward ? TYPE_REWARD : TYPE_LOAD_MORE);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        if(holder.type == TYPE_REWARD){
            holder.title.setText(mData.get(position).getTitle());
            holder.date.setText(Helper.longToStringDateFormat(mData.get(position).getDate()));

            Picasso.with(mContext)
                    .load(mData.get(position).getUrl_image())
                    .into(holder.imageView);

            holder.rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DetailRewardActivity.startThisActivity(mContext, mData.get(position));
                }
            });

        }else{
            holder.loadmore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holder.loadmore.setVisibility(View.GONE);
                    holder.progressBar.setVisibility(View.VISIBLE);
                    callback.onClickLoadmore(holder.progressBar, holder.loadmore);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mData != null ? mData.size() + 1 : 0;
    }

    @Override
    public int getItemViewType(int position) {
        if(position != mData.size()){
            return R.layout.row_reward;
        }else{
            return R.layout.loadmore_nocard_row;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        View rootView;
        int type = TYPE_REWARD;
        ImageView imageView;
        ProgressBar progressBar;
        TextView title, date, loadmore;

        public ViewHolder(View v, int type) {
            super(v);
            rootView = v;
            this.type = type;

            if(type == TYPE_REWARD){
                imageView = (ImageView)v.findViewById(R.id.img_reward_row);
                title = (TextView)v.findViewById(R.id.content_reward_row);
                date = (TextView)v.findViewById(R.id.title_reward_row);
            }else{
                loadmore = (TextView)v.findViewById(R.id.load_more);
                progressBar = (ProgressBar) v.findViewById(R.id.progress_bar);
            }
        }
    }
}
