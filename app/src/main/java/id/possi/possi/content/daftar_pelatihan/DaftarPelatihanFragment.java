package id.possi.possi.content.daftar_pelatihan;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import id.possi.possi.R;
import id.possi.possi.content.register_pelatihan.PelatihanAdapter;
import id.possi.possi.dao.PelatihanDao;
import id.possi.possi.utils.LoadMoreCallback;


public class DaftarPelatihanFragment extends Fragment implements LoadMoreCallback {

    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private PelatihanAdapter adapter;
    private List<PelatihanDao> mData = new ArrayList<>();

    public DaftarPelatihanFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_register_pelatihan, container, false);
        initComponent(v);
        return v;
    }

    private void initComponent(View v) {
        recyclerView = (RecyclerView)v.findViewById(R.id.rc_pelatihan);
        layoutManager = new LinearLayoutManager(getContext());
        adapter = new PelatihanAdapter(this, mData);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        mData.add(new PelatihanDao("1","Pelatihan Bersama", "Bandung", getString(R.string.long_text), "http://bandung.panduanwisata.id/files/2013/03/kolam-renang-bandung2.jpg","0831873262","Buah Batu", "bikasoga@mail.com","Bapak Sam", 30, -6.3212, 107.3213));
        mData.add(new PelatihanDao("1","Pelatihan Bersama", "Bandung", getString(R.string.long_text), "http://bandung.panduanwisata.id/files/2013/03/kolam-renang-bandung2.jpg","0831873262","Buah Batu", "bikasoga@mail.com","Bapak Sam", 30, -6.3212, 107.3213));
        mData.add(new PelatihanDao("1","Pelatihan Bersama", "Bandung", getString(R.string.long_text), "http://bandung.panduanwisata.id/files/2013/03/kolam-renang-bandung2.jpg","0831873262","Buah Batu", "bikasoga@mail.com","Bapak Sam", 30, -6.3212, 107.3213));
        mData.add(new PelatihanDao("1","Pelatihan Bersama", "Bandung", getString(R.string.long_text), "http://bandung.panduanwisata.id/files/2013/03/kolam-renang-bandung2.jpg","0831873262","Buah Batu", "bikasoga@mail.com","Bapak Sam", 30, -6.3212, 107.3213));
        mData.add(new PelatihanDao("1","Pelatihan Bersama", "Bandung", getString(R.string.long_text), "http://bandung.panduanwisata.id/files/2013/03/kolam-renang-bandung2.jpg","0831873262","Buah Batu", "bikasoga@mail.com","Bapak Sam", 30, -6.3212, 107.3213));

        adapter.notifyDataSetChanged();
    }

    @Override
    public void onClickLoadmore(final ProgressBar progressBar, final TextView loadmore) {

        new Handler().postDelayed(new Runnable() {
            /**
             * setelah 3s
             */
            @Override
            public void run() {
                progressBar.setVisibility(View.GONE);
                loadmore.setVisibility(View.VISIBLE);
                loadMore();
            }
        }, 3000);
    }

    private void loadMore() {
        mData.add(new PelatihanDao("1","Pelatihan Bersama", "Bandung", getString(R.string.long_text), "http://bandung.panduanwisata.id/files/2013/03/kolam-renang-bandung2.jpg","0831873262","Buah Batu", "bikasoga@mail.com","Bapak Sam", 30, -6.3212, 107.3213));
        mData.add(new PelatihanDao("1","Pelatihan Bersama", "Bandung", getString(R.string.long_text), "http://bandung.panduanwisata.id/files/2013/03/kolam-renang-bandung2.jpg","0831873262","Buah Batu", "bikasoga@mail.com","Bapak Sam", 30, -6.3212, 107.3213));
        mData.add(new PelatihanDao("1","Pelatihan Bersama", "Bandung", getString(R.string.long_text), "http://bandung.panduanwisata.id/files/2013/03/kolam-renang-bandung2.jpg","0831873262","Buah Batu", "bikasoga@mail.com","Bapak Sam", 30, -6.3212, 107.3213));
    }
}
