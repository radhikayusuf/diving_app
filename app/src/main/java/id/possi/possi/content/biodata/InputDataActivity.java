package id.possi.possi.content.biodata;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import id.possi.possi.R;
import id.possi.possi.content.main.MainActivity;
import id.possi.possi.content.sign_up.RegisterActivity;
import id.possi.possi.utils.MyDialog;

public class InputDataActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar toolbar;
    private TextInputEditText date_tanggal_lahir;
    private TextInputEditText edit_provinsi;
    private TextInputLayout tanggal_lahir;
    private TextInputLayout provinsi;
    private TextView tv_date, tv_provinsi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_data);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initComponent();

        date_tanggal_lahir.setText((new SimpleDateFormat("dd/MM/yyyy")).format(new Date()));



        getSupportActionBar().setTitle("Data Diri");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void initComponent() {
        date_tanggal_lahir = (TextInputEditText) findViewById(R.id.edit_tanggal_lahir);
        edit_provinsi = (TextInputEditText) findViewById(R.id.edit_provinsi);
        tanggal_lahir = (TextInputLayout) findViewById(R.id.tanggal_lahir);
        provinsi = (TextInputLayout) findViewById(R.id.provinsi);


        tv_date = (TextView) findViewById(R.id.tv_date);
        tv_provinsi = (TextView) findViewById(R.id.tv_provinsi);

        date_tanggal_lahir.setOnClickListener(this);
        tanggal_lahir.setOnClickListener(this);
        edit_provinsi.setOnClickListener(this);
        provinsi.setOnClickListener(this);
        tv_date.setOnClickListener(this);
        tv_provinsi.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void gotoMain(View view) {
        startActivity(new Intent(InputDataActivity.this, MainActivity.class));
    }


    @Override
    public void onClick(View view) {

        if(view.getId() == R.id.tanggal_lahir || (view.getId() == R.id.edit_tanggal_lahir || view.getId() == R.id.tv_date)){
            String date[] = date_tanggal_lahir.getText().toString().split("/");


            DatePickerDialog datePicker = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                    date_tanggal_lahir.setText(String.valueOf((day < 10 ? "0"+day : day)+"/"+((month+1 < 10 ? "0"+(month+1) : ""+(month+1))+"/"+year)));
                }
            },  Integer.parseInt(date[2]), Integer.parseInt(date[1])-1 ,Integer.parseInt(date[0]));
            datePicker.show();
        }else{
            AlertDialog.Builder builderSingle = new AlertDialog.Builder(InputDataActivity.this);
            builderSingle.setTitle("Pilih Provinsi");

            final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(InputDataActivity.this, android.R.layout.select_dialog_singlechoice);
            arrayAdapter.add("Jawa Barat");
            arrayAdapter.add("Jawa Tengah");
            arrayAdapter.add("Jawa Timur");


            builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String strName = arrayAdapter.getItem(which);
                    edit_provinsi.setText(strName);
//                    AlertDialog.Builder builderInner = new AlertDialog.Builder(InputDataActivity.this);
//                    builderInner.setMessage(strName);
//                    builderInner.setTitle("Your Selected Item is");
//                    builderInner.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog,int which) {
//                            dialog.dismiss();
//                        }
//                    });
//                    builderInner.show();
                }
            });
            builderSingle.show();
        }
    }
}
