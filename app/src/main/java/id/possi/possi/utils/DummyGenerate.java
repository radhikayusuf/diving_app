package id.possi.possi.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Radhika Yusuf on 26/05/17.
 */

public class DummyGenerate {

    private static final List<String> provinsi = new ArrayList<String>() {{
        add("--");
        add("Nanggro Aceh Darussalam");
        add("Sumatera Utara");
        add("Sumatera Barat");
        add("Riau");
        add("Kepulauan Riau");
        add("Jambi");
        add("Sumatera Selatan");
        add("Bangka Belitung");
        add("Bengkulu");
        add("Lampung");
        add("DKI Jakarta");
        add("Jawa Barat");
        add("Banten");
        add("Jawa Tengah");
        add("Daerah Istimewa Yogyakarta");
        add("Jawa Timur");
        add("Bali");
        add("Nusa Tenggara Barat");
        add("Nusa Tenggara Timur");
        add("Kalimantan Barat");
        add("Kalimantan Tengah");
        add("Kalimantan Selatan");
        add("Kalimantan Timur");
        add("Kalimantan Utara");
        add("Sulawesi Utara");
        add("Sulawesi Barat");
        add("Sulawesi Tengah");
        add("Sulawesi Tenggara");
        add("Sulawesi Selatan");
        add("Gorontalo");
        add("Maluku");
        add("Maluku Utara");
        add("Papua Barat");
        add("Papua");
    }};

    public static List<String> getDataDummyProvinsi(){
        return provinsi;
    }

}
