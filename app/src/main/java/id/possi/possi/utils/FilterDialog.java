package id.possi.possi.utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import id.possi.possi.R;

/**
 * Created by Radhika Yusuf on 26/05/17.
 */

public class FilterDialog extends DialogFragment{
    private int mSelectedPos = 0;
    private List<String> mData = new ArrayList<>();
    private String mSelectedItem = null;
    private String dialogTitle = null;
    private Spinner s;
    private DialogCallback evt;
    private int type_dialog = 0;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = LayoutInflater.from(getActivity());

        View v = inflater.inflate(R.layout.filter_dialog, null);

        builder.setView(v);
        builder.setTitle(dialogTitle != null ? dialogTitle : "Filter");
        mData = DummyGenerate.getDataDummyProvinsi();

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_item, mData);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                mSelectedItem = s != null ? s.getSelectedItem().toString() : "";
                mSelectedPos = s.getSelectedItemPosition();
                if(evt != null) evt.onSelectedSpinner(mSelectedItem, type_dialog);
            }
        });


        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
                //dialogInterface.dismiss();
            }
        });


        Dialog d = builder.create();
        s = (Spinner) v.findViewById(R.id.dialog_filter);
        s.setAdapter(adapter);

        s.setSelection(mSelectedPos);

        return d;
    }


    public void setmData(List<String> mData) {
        this.mData = mData;
    }

    public String getmSelectedItem() {
        return mSelectedItem;
    }

    public void setmSelectedItem(String mSelectedItem) {
        this.mSelectedItem = mSelectedItem;
    }

    public String getDialogTitle() {
        return dialogTitle;
    }

    public void setDialogTitle(String dialogTitle) {
        this.dialogTitle = dialogTitle;
    }

    public void setEvt(DialogCallback evt) {
        this.evt = evt;
    }

    public void setType_dialog(int type_dialog) {
        this.type_dialog = type_dialog;
    }

    public interface DialogCallback{
        void onSelectedSpinner(String text, int dialog_type);
    }
}
