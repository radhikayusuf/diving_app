package id.possi.possi.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import id.possi.possi.content.main.MainActivity;

/**
 * Created by root on 27/04/17.
 */

public class Helper {

    public static String  longToStringDateFormat(Long date){
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, MMM dd yy, HH:mm", Locale.getDefault());
        Date buff_date = new Date();
        buff_date.setTime(date);

        return (String.valueOf(dateFormat.format(buff_date)));
    }

    public static String  longToStringCustomDateFormat(Long date, String format){
        SimpleDateFormat dateFormat = new SimpleDateFormat(format, Locale.getDefault());
        Date buff_date = new Date();
        buff_date.setTime(date);

        return (String.valueOf(dateFormat.format(buff_date)));
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {

        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(String path) {
        int reqWidth = 300;
        int reqHeight = 300;

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(path, options);
    }


}
