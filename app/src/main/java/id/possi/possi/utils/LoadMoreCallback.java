package id.possi.possi.utils;

import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * Created by root on 07/05/17.
 */

public interface LoadMoreCallback {
    void onClickLoadmore(ProgressBar progressBar, TextView loadmore);
}
