package id.possi.possi.utils;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;

import id.possi.possi.R;

/**
 * Created by root on 14/04/17.
 */

public class MyDialog {

    private Context mContext;
    private String result = null;

    private static int state = 0;

    public MyDialog(Context mContext) {
        this.mContext = mContext;
    }

    public String getDate(String param_date){
        result = null;
        String date[] = param_date.split("/");


        DatePickerDialog datePicker = new DatePickerDialog(mContext, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                 result = String.valueOf((day < 10 ? "0"+day : day)+"/"+((month+1 < 10 ? "0"+(month+1) : ""+(month+1))+"/"+year));
            }
        },  Integer.parseInt(date[2]), Integer.parseInt(date[1])-1 ,Integer.parseInt(date[0]));
        datePicker.show();

        return result;
    }

    public String getProvinsi(){
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(mContext);
//        builderSingle.setIcon(R.drawable.ic_launcher);
        builderSingle.setTitle("Pilih Provinsi");

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(mContext, android.R.layout.select_dialog_singlechoice);
        arrayAdapter.add("Jawa Barat");
        arrayAdapter.add("Jawa Tengah");
        arrayAdapter.add("Jawa Timur");


        builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String strName = arrayAdapter.getItem(which);
                AlertDialog.Builder builderInner = new AlertDialog.Builder(mContext);
                builderInner.setMessage(strName);
                result = strName;
                builderInner.setTitle("Your Selected Item is");
                builderInner.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog,int which) {
                        dialog.dismiss();
                    }
                });
                builderInner.show();
            }
        });
        builderSingle.show();


        return result;
    }


    public static void showGantiStatusDialog(final Context context, String title, boolean status, final CallbackDialogStatus evt, final int pos){

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        builder.setTitle("Status Kelulusan "+title);

        final ArrayAdapter<String> adapter = new ArrayAdapter<>(context, android.R.layout.select_dialog_singlechoice);
        adapter.add("Lulus");
        adapter.add("Tidak Lulus");

        builder.setSingleChoiceItems(adapter, status ? 0 : 1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                state = i;
            }
        });

        builder.setPositiveButton("SAVE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                evt.onChangeStatus(pos, adapter.getItem(state).equalsIgnoreCase("Lulus"));
            }
        });

        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        android.app.AlertDialog dialog = builder.create();
        dialog.show();
    }


    public static void showNewGantiStatusDialog(final Context context, String title, boolean status, final CallbackDialogStatus evt, final int pos){

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        builder.setTitle("Status Kelulusan "+title);

        final ArrayAdapter<String> adapter = new ArrayAdapter<>(context, android.R.layout.select_dialog_singlechoice);
        adapter.add("Tidak Lulus");
        adapter.add("Lulus");

        builder.setSingleChoiceItems(adapter, status ? 0 : 1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                state = i;
            }
        });

        builder.setPositiveButton("SAVE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                evt.onChangeStatus(pos, adapter.getItem(state).equalsIgnoreCase("Tidak Lulus"));
            }
        });

        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        android.app.AlertDialog dialog = builder.create();
        dialog.show();
    }



    public interface CallbackDialogStatus{
        void onChangeStatus(int pos, boolean status);
    }
}
