package id.possi.possi.dao;

/**
 * Created by root on 03/05/17.
 */

public class EventDao {
    private String id, title, location_desc, desc, img, location_title, location_place, lokasi_provinsi;
    private long datetime;
    private double lat, lang;

    public EventDao(String id, String title, String location_desc, String desc, String img, String location_title, String location_place, String lokasi_provinsi, long datetime, double lat, double lang) {
        this.id = id;
        this.title = title;
        this.location_desc = location_desc;
        this.desc = desc;
        this.img = img;
        this.location_title = location_title;
        this.location_place = location_place;
        this.lokasi_provinsi = lokasi_provinsi;
        this.datetime = datetime;
        this.lat = lat;
        this.lang = lang;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLocation_desc() {
        return location_desc;
    }

    public void setLocation_desc(String location_desc) {
        this.location_desc = location_desc;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getLocation_title() {
        return location_title;
    }

    public void setLocation_title(String location_title) {
        this.location_title = location_title;
    }

    public String getLocation_place() {
        return location_place;
    }

    public void setLocation_place(String location_place) {
        this.location_place = location_place;
    }

    public long getDatetime() {
        return datetime;
    }

    public void setDatetime(long datetime) {
        this.datetime = datetime;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLang() {
        return lang;
    }

    public void setLang(double lang) {
        this.lang = lang;
    }

    public String getLokasi_provinsi() {
        return lokasi_provinsi;
    }

    public void setLokasi_provinsi(String lokasi_provinsi) {
        this.lokasi_provinsi = lokasi_provinsi;
    }
}
