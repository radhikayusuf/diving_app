package id.possi.possi.dao;

/**
 * Created by root on 11/04/17.
 */

public class DaftarSiswaDao {
    private int id;
    private String nama_lengkap, email, jenis_kelamin, tanggal_lahir, tempat_lahir, alamat, provinsi_asal, image_url;
    private boolean status_lunas;

    public DaftarSiswaDao(int id, String nama_lengkap, String email, String jenis_kelamin, String tanggal_lahir, String tempat_lahir, String alamat, String provinsi_asal, String image_url, boolean status_lunas) {
        this.id = id;
        this.nama_lengkap = nama_lengkap;
        this.email = email;
        this.jenis_kelamin = jenis_kelamin;
        this.tanggal_lahir = tanggal_lahir;
        this.tempat_lahir = tempat_lahir;
        this.alamat = alamat;
        this.provinsi_asal = provinsi_asal;
        this.image_url = image_url;
        this.status_lunas = status_lunas;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama_lengkap() {
        return nama_lengkap;
    }

    public void setNama_lengkap(String nama_lengkap) {
        this.nama_lengkap = nama_lengkap;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getJenis_kelamin() {
        return jenis_kelamin;
    }

    public void setJenis_kelamin(String jenis_kelamin) {
        this.jenis_kelamin = jenis_kelamin;
    }

    public String getTanggal_lahir() {
        return tanggal_lahir;
    }

    public void setTanggal_lahir(String tanggal_lahir) {
        this.tanggal_lahir = tanggal_lahir;
    }

    public String getTempat_lahir() {
        return tempat_lahir;
    }

    public void setTempat_lahir(String tempat_lahir) {
        this.tempat_lahir = tempat_lahir;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getProvinsi_asal() {
        return provinsi_asal;
    }

    public void setProvinsi_asal(String provinsi_asal) {
        this.provinsi_asal = provinsi_asal;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public boolean isStatus_lunas() {
        return status_lunas;
    }

    public void setStatus_lunas(boolean status_lunas) {
        this.status_lunas = status_lunas;
    }
}