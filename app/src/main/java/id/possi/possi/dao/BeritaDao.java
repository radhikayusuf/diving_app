package id.possi.possi.dao;

/**
 * Created by root on 11/04/17.
 */

public class BeritaDao {
    private int id;
    private long date;
    private String creator, title, url_image, content, lokasi_provinsi;

    public BeritaDao(int id, long date, String creator, String title, String url_image, String content, String lokasi_provinsi) {
        this.id = id;
        this.date = date;
        this.creator = creator;
        this.title = title;
        this.url_image = url_image;
        this.content = content;
        this.lokasi_provinsi = lokasi_provinsi;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl_image() {
        return url_image;
    }

    public void setUrl_image(String url_image) {
        this.url_image = url_image;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getLokasi_provinsi() {
        return lokasi_provinsi;
    }

    public void setLokasi_provinsi(String lokasi_provinsi) {
        this.lokasi_provinsi = lokasi_provinsi;
    }
}
