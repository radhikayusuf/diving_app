package id.possi.possi.dao;

/**
 * Created by root on 04/05/17.
 */

public class DiveCenterDao {
    private String id, title, kota, desc, img_url, no_telp, alamat, email, penanggung_jawab;
    private int distance;
    private Double lat, lang;

    public DiveCenterDao(String id, String title, String kota, String desc, String img_url, String no_telp, String alamat, String email, String penanggung_jawab, int distance, Double lat, Double lang) {
        this.id = id;
        this.title = title;
        this.kota = kota;
        this.desc = desc;
        this.img_url = img_url;
        this.no_telp = no_telp;
        this.alamat = alamat;
        this.email = email;
        this.penanggung_jawab = penanggung_jawab;
        this.distance = distance;
        this.lat = lat;
        this.lang = lang;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getKota() {
        return kota;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public String getNo_telp() {
        return no_telp;
    }

    public void setNo_telp(String no_telp) {
        this.no_telp = no_telp;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPenanggung_jawab() {
        return penanggung_jawab;
    }

    public void setPenanggung_jawab(String penanggung_jawab) {
        this.penanggung_jawab = penanggung_jawab;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLang() {
        return lang;
    }

    public void setLang(Double lang) {
        this.lang = lang;
    }
}
