package id.possi.possi.dao;

/**
 * Created by root on 12/05/17.
 */

public class ProfileDao {
    private String name, email, password, provinsi, no_anggota, alamat, phone, kota, token, tempat_lahir, image_url, no_ktp;
    private int id_member, kode_aktivasi;
    private boolean email_status;
    private Long tanggal_lahir;

    public ProfileDao(String name, String email, String password, String provinsi, String no_anggota, String alamat, String phone, String kota, String token, String tempat_lahir, String image_url, String no_ktp, int id_member, int kode_aktivasi, boolean email_status, Long tanggal_lahir) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.provinsi = provinsi;
        this.no_anggota = no_anggota;
        this.alamat = alamat;
        this.phone = phone;
        this.kota = kota;
        this.token = token;
        this.tempat_lahir = tempat_lahir;
        this.image_url = image_url;
        this.no_ktp = no_ktp;
        this.id_member = id_member;
        this.kode_aktivasi = kode_aktivasi;
        this.email_status = email_status;
        this.tanggal_lahir = tanggal_lahir;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getProvinsi() {
        return provinsi;
    }

    public void setProvinsi(String provinsi) {
        this.provinsi = provinsi;
    }

    public String getNo_anggota() {
        return no_anggota;
    }

    public void setNo_anggota(String no_anggota) {
        this.no_anggota = no_anggota;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getKota() {
        return kota;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTempat_lahir() {
        return tempat_lahir;
    }

    public void setTempat_lahir(String tempat_lahir) {
        this.tempat_lahir = tempat_lahir;
    }

    public int getId_member() {
        return id_member;
    }

    public void setId_member(int id_member) {
        this.id_member = id_member;
    }

    public int getKode_aktivasi() {
        return kode_aktivasi;
    }

    public void setKode_aktivasi(int kode_aktivasi) {
        this.kode_aktivasi = kode_aktivasi;
    }

    public boolean isEmail_status() {
        return email_status;
    }

    public void setEmail_status(boolean email_status) {
        this.email_status = email_status;
    }

    public Long getTanggal_lahir() {
        return tanggal_lahir;
    }

    public void setTanggal_lahir(Long tanggal_lahir) {
        this.tanggal_lahir = tanggal_lahir;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getNo_ktp() {
        return no_ktp;
    }

    public void setNo_ktp(String no_ktp) {
        this.no_ktp = no_ktp;
    }
}
