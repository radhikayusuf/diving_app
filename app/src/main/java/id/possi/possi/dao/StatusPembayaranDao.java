package id.possi.possi.dao;

/**
 * Created by root on 12/04/17.
 */

public class StatusPembayaranDao {
    private int id, total_pelatihan;
    private String nama_pelatihan, deskripsi;
    private boolean lunas;

    public StatusPembayaranDao(int id, int total_pelatihan, String nama_pelatihan, String deskripsi, boolean lunas) {
        this.id = id;
        this.total_pelatihan = total_pelatihan;
        this.nama_pelatihan = nama_pelatihan;
        this.deskripsi = deskripsi;
        this.lunas = lunas;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTotal_pelatihan() {
        return total_pelatihan;
    }

    public void setTotal_pelatihan(int total_pelatihan) {
        this.total_pelatihan = total_pelatihan;
    }

    public String getNama_pelatihan() {
        return nama_pelatihan;
    }

    public void setNama_pelatihan(String nama_pelatihan) {
        this.nama_pelatihan = nama_pelatihan;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public boolean isLunas() {
        return lunas;
    }

    public void setLunas(boolean lunas) {
        this.lunas = lunas;
    }
}
